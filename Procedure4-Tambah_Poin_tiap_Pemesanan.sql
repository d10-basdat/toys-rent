CREATE OR REPLACE FUNCTION
tambah_poin_pemesanan()
RETURNS trigger AS
$$
    DECLARE

        jumlah_poin INTEGER;

    BEGIN

        IF(TG_OP = 'INSERT') THEN

            SELECT kuantitas_barang*100 INTO jumlah_poin
            FROM PEMESANAN P
            WHERE  P.id_customer = NEW.id_customer;

            UPDATE ANGGOTA
            SET poin = poin + jumlah_poin
            WHERE no_ktp = NEW.no_ktp_pemesanan;

            RETURN NEW;
        

        ELSIF (TG_OP = 'DELETE') THEN

            SELECT kuantitas_barang*100 INTO jumlah_poin
            FROM PEMESANAN P
            WHERE  P.id_customer = OLD.id_customer;

            UPDATE ANGGOTA
            SET poin = poin - jumlah_poin
            WHERE no_ktp = OLD.no_ktp_pemesanan;    

            RETURN OLD;
            
        
        ELSIF (TG_OP = 'UPDATE') THEN

            SELECT kuantitas_barang*100 INTO jumlah_poin
            FROM PEMESANAN P
            WHERE  P.id_customer = OLD.id_customer;

            UPDATE ANGGOTA
            SET poin = poin - jumlah_poin
            WHERE no_ktp = OLD.no_ktp_pemesanan;

            UPDATE ANGGOTA
            SET poin = poin + jumlah_poin
            WHERE no_ktp = NEW.no_ktp_pemesanan;

            RETURN NEW;
            
            
        END IF;
    END;
$$
LANGUAGE plpgsql;