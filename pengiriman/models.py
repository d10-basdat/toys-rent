from django.db import models

# Create your models here.
class Pengiriman(models.Model):
    no_resi = models.CharField(max_length = 10, primary_key=True)
    id_pemesanan = models.CharField(max_length = 10)
    metode = models.TextField()
    ongkos = models.FloatField()
    tanggal = models.DateField()
    no_ktp_anggota = models.CharField(max_length = 20)
    nama_alamat_anggota = models.CharField(max_length = 255)
