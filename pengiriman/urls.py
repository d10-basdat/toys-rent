from django.urls import path
from .views import *

urlpatterns = [
    path('', list_pengiriman, name='list_pengiriman'),
    path('update/', update_pengiriman, name='update_pengiriman'),
    path('buat/', buat_pengiriman, name='buat_pengiriman'),
    path('buatPengirimanFormSubmit/', buatPengirimanFormSubmit, name='buatPengirimanFormSubmit'),
]
