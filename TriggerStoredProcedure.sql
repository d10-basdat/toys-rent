-- 3
-- TRIGGER
CREATE TRIGGER CHANGE_KONDISI_BARANG
AFTER INSERT
ON PEMESANAN
FOR EACH ROW
EXECUTE PROCEDURE UPDATE_STATUS_BARANG();

-- STORED PROCEDURE
CREATE OR REPLACE FUNCTION UPDATE_STATUS_BARANG()
RETURNS TRIGGER AS
$$
DECLARE
    new_kondisi text;
    nama_penyewa varchar(255);
BEGIN
    SELECT A.nama_lengkap INTO nama_penyewa
    FROM ANGGOTA A
    WHERE NEW.no_ktp_pemesan = A.no_ktp;

    new_kondisi := 'sedang disewa %', nama_penyewa;

    UPDATE BARANG 
    SET kondisi = new_kondisi
    WHERE id_pemesanan IN (
    	SELECT NEW.id_pemesanan
    	FROM BARANG_PESANAN BP, BARANG B
    	WHERE NEW.id_pemesanan = BP.id_pemesanan
    	AND BP.id_barang = B.id_barang);
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;


-- 4
-- TRIGGER
CREATE TRIGGER UPDATE_POIN_SEWA
AFTER INSERT OR DELETE OR UPDATE OF no_ktp_pemesan, kuantitas_barang
ON PEMESANAN
FOR EACH ROW
EXECUTE PROCEDURE POIN_UPDATE_SEWA();

CREATE TRIGGER UPDATE_POIN_FROM_DEPOSITO
AFTER INSERT OR DELETE OR UPDATE OF no_ktp_penyewa
ON BARANG
FOR EACH ROW
EXECUTE PROCEDURE POIN_UPDATE_DEPOSITO();

-- STORED PROCEDURE
CREATE OR REPLACE FUNCTION POIN_UPDATE_SEWA()
RETURNS TRIGGER AS
$$
DECLARE
	poin_new REAL;
BEGIN
	IF(TG_OP = 'INSERT') THEN
		SELECT (100*NEW.kuantitas_barang) INTO poin_new
		FROM ANGGOTA A
		WHERE NEW.no_ktp_pemesan = A.no_ktp;

		UPDATE ANGGOTA A
		SET poin = poin + poin_new
		WHERE NEW.no_ktp_pemesan = A.no_ktp;
		RETURN NEW;

	ELSIF(TG_OP = 'DELETE') THEN
		SELECT (100*NEW.kuantitas_barang) INTO poin_new
		FROM ANGGOTA A
		WHERE OLD.no_ktp_pemesan = A.no_ktp_pemesan;

		UPDATE ANGGOTA A
		SET poin = poin - poin_new
		WHERE OLD.no_ktp_pemesan = A.no_ktp;
		RETURN OLD;

	ELSE
		IF(NEW.no_ktp_pemesan <> OLD.no_ktp_pemesan) THEN
			SELECT (100*NEW.kuantitas_barang) INTO poin_new
			FROM ANGGOTA A
			WHERE NEW.no_ktp_pemesan = A.no_ktp;

			UPDATE ANGGOTA A
			SET poin = poin + poin_new
			WHERE NEW.no_ktp_pemesan = A.no_ktp;

			UPDATE ANGGOTA A
			SET poin = poin - poin_new
			WHERE OLD.no_ktp_pemesan = A.no_ktp;
		
		ELSE
			SELECT (100*(NEW.kuantitas_barang - OLD.kuantitas_barang)) INTO poin_new
			FROM ANGGOTA A
			WHERE NEW.no_ktp_pemesan = A.no_ktp;

			UPDATE ANGGOTA A
			SET poin = poin + poin_new
			WHERE NEW.no_ktp_pemesan = A.no_ktp;
		END IF;
		RETURN NEW;
	END IF;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION POIN_UPDATE_DEPOSITO()
RETURNS TRIGGER AS
$$
BEGIN
	IF(TG_OP = 'INSERT') THEN
	UPDATE ANGGOTA A
	SET poin = poin + 100
	WHERE NEW.no_ktp_penyewa = A.no_ktp;
	RETURN NEW;

	ELSIF(TG_OP = 'DELETE') THEN
	UPDATE ANGGOTA A
	SET poin = poin - 100
	WHERE OLD.no_ktp_penyewa = A.no_ktp_penyewa;
	RETURN OLD;

	ELSE
	UPDATE ANGGOTA A
	SET poin = poin + 100
	WHERE NEW.no_ktp_penyewa = A.no_ktp;

	UPDATE ANGGOTA A
	SET poin = poin - 100
	WHERE OLD.no_ktp_penyewa = A.no_ktp_penyewa;
	RETURN NEW;
	END IF;
END;
$$
LANGUAGE plpgsql;