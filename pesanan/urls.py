from django.urls import path
from .views import *

urlpatterns = [
    path('', list_pesanan, name='list_pesanan'),
    path('buat-pesanan/', buat_pesanan, name='buat_pesanan'),
    path('update-pesanan/', update_pesanan, name='update_pesanan')
]
