from django.conf.urls import url
from django.urls import path
from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', list_review, name='list_review'),
    path('buat', buat_review, name='buat_review'),
    path('update', update_review, name='update_review'),
]
