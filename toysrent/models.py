from django.db import models

# Create your models here.
class Admin(models.Model):
    no_ktp = models.CharField(max_length = 20, primary_key=True)

class Anggota(models.Model):
    no_ktp = models.CharField(max_length = 20, primary_key=True)
    poin = models.FloatField()
    level = models.CharField(max_length = 20)

class Barang(models.Model):
    id_barang = models.CharField(max_length = 10, primary_key=True)
    nama_item = models.CharField(max_length = 255)
    warna = models.CharField(max_length = 50)
    url_foto = models.TextField()
    lama_penggunaan = models.IntegerField()
    no_ktp_penyewa = models.CharField(max_length = 20)

# class BarangDikembalikan(models.Model):

# class BarangDikirim(models.Model):

# class BarangPesanan(models.Model):

# class Chat(models.Model):

# class InfoBarangLevel(models.Model):

# class Item(models.Model):

# class Kategori(models.Model):

# class KategoriItem(models.Model):

# class LevelKeanggotaan(models.Model):

# class Pemesanan(models.Model):

# class Pengembalian(models.Model):

# class Pengguna(models.Model):

# class Pengiriman(models.Model):
class Pengiriman(models.Model):
    no_resi = models.CharField(max_length = 10, primary_key=True)
    id_pemesanan = models.CharField(max_length = 10)
    metode = models.TextField()
    ongkos = models.FloatField()
    tanggal = models.DateField()
    no_ktp_anggota = models.CharField(max_length = 20)
    nama_alamat_anggota = models.CharField(max_length = 255)

# class Status(models.Model):
