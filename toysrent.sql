--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 10.7 (Ubuntu 10.7-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: toysrent; Type: SCHEMA; Schema: -; Owner: db2018054
--

CREATE SCHEMA toysrent;


ALTER SCHEMA toysrent OWNER TO db2018054;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: tambah_poin_pemesanan(); Type: FUNCTION; Schema: toysrent; Owner: db2018054
--

CREATE FUNCTION toysrent.tambah_poin_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE

        jumlah_poin INTEGER;

    BEGIN

        IF(TG_OP = 'INSERT') THEN

            SELECT kuantitas_barang*100 INTO jumlah_poin
            FROM PEMESANAN P
            WHERE  P.id_customer = NEW.id_customer;

            UPDATE ANGGOTA
            SET poin = poin + jumlah_poin
            WHERE no_ktp = NEW.no_ktp_pemesanan;

            RETURN NEW;
        

        ELSIF (TG_OP = 'DELETE') THEN

            SELECT kuantitas_barang*100 INTO jumlah_poin
            FROM PEMESANAN P
            WHERE  P.id_customer = OLD.id_customer;

            UPDATE ANGGOTA
            SET poin = poin - jumlah_poin
            WHERE no_ktp = OLD.no_ktp_pemesanan;    

            RETURN OLD;
            
        
        ELSIF (TG_OP = 'UPDATE') THEN

            SELECT kuantitas_barang*100 INTO jumlah_poin
            FROM PEMESANAN P
            WHERE  P.id_customer = OLD.id_customer;

            UPDATE ANGGOTA
            SET poin = poin - jumlah_poin
            WHERE no_ktp = OLD.no_ktp_pemesanan;

            UPDATE ANGGOTA
            SET poin = poin + jumlah_poin
            WHERE no_ktp = NEW.no_ktp_pemesanan;

            RETURN NEW;
            
            
        END IF;
    END;
$$;


ALTER FUNCTION toysrent.tambah_poin_pemesanan() OWNER TO db2018054;

--
-- Name: update_kondisi(); Type: FUNCTION; Schema: toysrent; Owner: db2018054
--

CREATE FUNCTION toysrent.update_kondisi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

BEGIN 
IF (TG_OP = 'INSERT') THEN
UPDATE BARANG
SET kondisi = 'sedang disewa anggota'
WHERE id_barang = NEW.id_barang;
RETURN NEW;

END IF;
END;
$$;


ALTER FUNCTION toysrent.update_kondisi() OWNER TO db2018054;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: barang_dikirim; Type: TABLE; Schema: public; Owner: db2018054
--



ALTER TABLE public.barang_dikirim OWNER TO db2018054;

--
-- Name: admin; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE toysrent.admin OWNER TO db2018054;

--
-- Name: alamat; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);


ALTER TABLE toysrent.alamat OWNER TO db2018054;

--
-- Name: anggota; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


ALTER TABLE toysrent.anggota OWNER TO db2018054;

--
-- Name: barang; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20)
);


ALTER TABLE toysrent.barang OWNER TO db2018054;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10)
);


ALTER TABLE toysrent.barang_dikembalikan OWNER TO db2018054;

--
-- Name: barang_dikirim; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL
);


ALTER TABLE toysrent.barang_dikirim OWNER TO db2018054;

--
-- Name: barang_pesanan; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    nama_status character varying(50) NOT NULL,
    status character varying(50)
);


ALTER TABLE toysrent.barang_pesanan OWNER TO db2018054;

--
-- Name: chat; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    datetime timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


ALTER TABLE toysrent.chat OWNER TO db2018054;

--
-- Name: info_barang_level; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_royalti real NOT NULL
);


ALTER TABLE toysrent.info_barang_level OWNER TO db2018054;

--
-- Name: item; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);


ALTER TABLE toysrent.item OWNER TO db2018054;

--
-- Name: kategori; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.kategori (
    nama character varying(255) NOT NULL,
    level integer NOT NULL,
    sub_dari character varying(255)
);


ALTER TABLE toysrent.kategori OWNER TO db2018054;

--
-- Name: kategori_item; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


ALTER TABLE toysrent.kategori_item OWNER TO db2018054;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE toysrent.level_keanggotaan OWNER TO db2018054;

--
-- Name: pemesanan; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


ALTER TABLE toysrent.pemesanan OWNER TO db2018054;

--
-- Name: pengembalian; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toysrent.pengembalian OWNER TO db2018054;

--
-- Name: pengguna; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE toysrent.pengguna OWNER TO db2018054;

--
-- Name: pengiriman; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    nama_alamat_anggota character varying(255) NOT NULL
);


ALTER TABLE toysrent.pengiriman OWNER TO db2018054;

--
-- Name: status; Type: TABLE; Schema: toysrent; Owner: db2018054
--

CREATE TABLE toysrent.status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


ALTER TABLE toysrent.status OWNER TO db2018054;

--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: public; Owner: db2018054
--

COPY public.barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
\.


--
-- Data for Name: admin; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.admin (no_ktp) FROM stdin;
3545822686710000
3535688087990000
3542466010800000
3569338224050000
3586660333050000
30365110892700
4905652105280000000
3565138551730000
5641821069610000
5100177121940000
3559383314010000
3573216731770000
633110105830000000
4905197192290000
3582940702190000
5048372224310000
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
3545822686710000	Vkrmymqjxfppgyupjvgaw	Eesscjlfgqxmofmvbhv	81	Changtang	16411.0
3535688087990000	Itkjlfdiqlsqsheektqyu	Byxerfvalyqjaomxrzj	6	Bergen	5806.0
3542466010800000	Wvucjatdotughtjydifxh	Symsxlffadcvtkxggfd	77	Jakubów	05-306
3569338224050000	Yqqblgznmgzlztljiyjtr	Qdjhrhmrdcwxlfapfca	46	’Unābah	12370.0
3586660333050000	Rbrftkmhrznbrhajxbrxh	Nrslxgtqpzbstuvarrh	9	Göteborg	16411
30365110892700	Uojxnotidqtnlztcvkapy	Oeiqgvofqdjgsmushvu	53	Floriana	5806
4905652105280000000	Lnbquklekmoqsgdwkogxi	Vmfljnsdlaqacpffrhe	56	Bandhagen	16411
3565138551730000	Ksmkxjzribzsoevahouex	Qmqdczczvslajwecmpk	37	Mosopa	5806
5641821069610000	Trizlcebdclvbostonwhj	Bdjnijhpmlboqlhaywc	68	Nancheng	16411
5100177121940000	Ozqbgrshvzhibocjocikk	Vboyhczkgtaatpsyzcc	83	Gombong	5806
3559383314010000	Btcbhdifdbgkajphdslhm	Hytedqgausqxwekbxhd	44	Thamaga	16411
3573216731770000	Lpiinbfirgihmcmpimseg	Nfzfgogxqebhzvswvag	67	Olofström	5806
633110105830000000	Qreafdsqmixyfciteymyx	Vmzfbkiixcjrysjucln	81	Kurjan	16411
4905197192290000	Qblqdbapbrqrjuhjmnnhv	Ciyleuuzmgwvusumqpi	32	Saitama	5806
3582940702190000	Tjgxugjqccmusboixckpg	Qwysbunvccjsxauneum	30	Pingshan	16411
5048372224310000	Zqhocdokjgvcuankiglcx	Mxkyyvfxustaxpzaqlh	98	Dubiecko	5806
67094986702100000	Akmvzukaooguxytmillyf	Htovpkgwsmjrudzyryh	75	Suqin Huimin	16411
3575871036560000	Rwdqvmvvxdnwlojyyxvhr	Vnhnafnnyvpkyzarekz	18	Laboulaye	5806
6759263453300000000	Iodmrcdphdisgswbqgylz	Mucesisyzvsffvpzxov	55	Kacangan Lor	16411
374622624522000	Hoxhyfhlletwkyjvlntke	Chydploxskjvcaatmad	3	Yelwa	5806
372301029043000	Vywzgcktlpzcerlabrydx	Atyzbuwkczwquwqwpuo	95	Gwoźnica Górna	16411
3545822686710000	Zqbirugexrmchfyqxryot	Rreklsugwstqjvgjfke	92	Taoyuan	5806
3535688087990000	Obldpcslwtdgxlruppuah	Bushgjhvyezziewpdsd	52	Kauman	16411
3542466010800000	Bkndbrqmydceecsmqdgqf	Aroamwanatdhflbbvsv	37	Korba	5806
3569338224050000	Cqjyowwxlehfdjuolhfmm	Yhnrfgkasffbyhxojwf	53	Vista Hermosa	16411
3586660333050000	Rjcnbkchiwcstvpeljdpd	Plnmmeswyemqbuiqlpv	36	Urozhaynoye	5806
30365110892700	Hqzovhwfzmsjiiejrfirf	Yetlmpjicsffkvnakcs	53	Kinango	16411
4905652105280000000	Cgbguzrqmjzvdcicefheq	Carddgkvzcipnnpmqoe	10	Ýdra	5806
3565138551730000	Ylzanqqrzlnlfenckocpp	Spiewmwfbjdgzvqvdfp	62	Miłosław	16411
5641821069610000	Yqbmdcaliwpuoyfkadidt	Stynzcdxtbvzvglzofs	92	Los Pinos	5806
5100177121940000	Itxargudltlziionxukzc	Zrlezpufszqireztvty	88	Yunlong	16411
3559383314010000	Hootbweruadrioczueuyv	Gryntyxmaajilecfsvq	48	Omaha	5806
3573216731770000	Xwwugkjtgiawguckcuhyj	Fjeunjrlcffzhjhqamj	28	Józefosław	16411
633110105830000000	Eitpqbqwvbkpcdxlgpgyg	Ocpmatsmwyiocfxkvwp	45	Hiratsuka	5806
4905197192290000	Yphrrgiohxlzizfptwait	Webszzdrgvxysrqdwtc	13	Jiazhi	16411
3582940702190000	Rftopdogvjehznvjybgha	Vsmivecjuaizxmhterv	24	Kutloanong	5806
5048372224310000	Cktsnzocjrjownchmpvly	Bfxzdgsbylbslkapvfw	55	Santa Lucia	16411
67094986702100000	Nkchivuyuocghwvothxxx	Julijhxkcjsvphrcspp	100	Paços	5806
3575871036560000	Lchoaqnzleygfnjkpnurf	Vmbgzybcsglrudjmztx	23	San Esteban	16411
6759263453300000000	Azpfgxskyddjrihurhquh	Mbkqnalskkjiptnfihy	80	Chahe	5806
374622624522000	Janlxroeknqobnjswuryc	Ogufibgvtqqiluajrcu	8	Oral	16411
372301029043000	Ziwdhdtrfdkghuhcjarnu	Celfcbvhfvpgowghtyb	24	Maria	5806
3545822686710000	Lbdzmlywaoxaqkmplriuq	Zgvbxabsifjsvltxnlh	17	Pocokan Satu	16411
3535688087990000	Zhtcgnstplykawehpxuuv	Mvzecaivcxbqgmafolm	46	Budapest	5806
3542466010800000	Djacsezvxivuaioomdvzy	Kzwabsasjyxycdjlzmh	36	Chusovoy	16411
3569338224050000	Hrlmlczxuzyxaippzvcdn	Kttywkycybayljhogld	78	Saumur	5806
3586660333050000	Lhjgefwptvakyjtjbdbft	Qtwplnxrazjqvvofvzb	69	Biloxi	16411
30365110892700	Uhhgwcdfnjhrgoymrpwlc	Xkesuwwxgdpgtgckcar	58	Torbat-e Jām	5806
4905652105280000000	Ozqpdthifbricoykqbjyx	Asqfulyzizvhscqcbuw	54	Oebai	16411
3565138551730000	Spjpsbhplarmfubfcyvxg	Fwnazvpcdipqfdiouvk	93	Jiukeng	5806
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.anggota (no_ktp, poin, level) FROM stdin;
3545822686710000	17.3999996	nN0YbZE1lM
3535688087990000	94.3000031	TTP7DI3I6O
3542466010800000	63.7999992	AIKWTIy9XU
3569338224050000	9.89999962	nN0YbZE1lM
3586660333050000	86	TTP7DI3I6O
30365110892700	19.2999992	AIKWTIy9XU
4905652105280000000	14.6999998	nN0YbZE1lM
3565138551730000	78.1999969	TTP7DI3I6O
5641821069610000	91.3000031	AIKWTIy9XU
5100177121940000	76.3000031	nN0YbZE1lM
3559383314010000	27.6000004	TTP7DI3I6O
3573216731770000	47.9000015	AIKWTIy9XU
633110105830000000	60.9000015	nN0YbZE1lM
4905197192290000	97.9000015	TTP7DI3I6O
3582940702190000	22	AIKWTIy9XU
5048372224310000	9.30000019	nN0YbZE1lM
67094986702100000	53.7999992	TTP7DI3I6O
3575871036560000	82.8000031	AIKWTIy9XU
6759263453300000000	41.9000015	nN0YbZE1lM
374622624522000	59.7000008	TTP7DI3I6O
372301029043000	48.5	AIKWTIy9XU
5602252082080000	55.7000008	nN0YbZE1lM
5602234304180000	27.5	TTP7DI3I6O
348159958884000	50.9000015	AIKWTIy9XU
374288379788000	58.7999992	nN0YbZE1lM
5387749970420000	96.5999985	TTP7DI3I6O
3569441237400000	27	AIKWTIy9XU
502043683482000000	77	nN0YbZE1lM
30436099908300	60.9000015	TTP7DI3I6O
6304943803570000	70.3000031	AIKWTIy9XU
5610443074650000	49.5999985	nN0YbZE1lM
374283591109000	80.1999969	TTP7DI3I6O
3543179044810000	42.9000015	AIKWTIy9XU
5602254999890000	78.3000031	nN0YbZE1lM
3543016151220000	93.0999985	TTP7DI3I6O
5602245175720000	84.9000015	AIKWTIy9XU
5137738202050000	25.1000004	nN0YbZE1lM
3581920451130000	13.8000002	TTP7DI3I6O
560222560864000000	95.5	AIKWTIy9XU
3588286776860000	16	nN0YbZE1lM
4844494273670000	52.5999985	TTP7DI3I6O
3565420812250000	65	AIKWTIy9XU
677197306217000000	69.5999985	nN0YbZE1lM
3580900625390000	74.1999969	TTP7DI3I6O
3588497997660000	84.3000031	AIKWTIy9XU
501842370868000000	88.6999969	nN0YbZE1lM
3587363223840000	42.4000015	TTP7DI3I6O
30169099037500	92.4000015	AIKWTIy9XU
201953385226000	85.4000015	nN0YbZE1lM
676709294748000000	17.1000004	TTP7DI3I6O
6709316972100000	32.2999992	AIKWTIy9XU
633483832358000000	62.7000008	nN0YbZE1lM
3585923020210000	46.5999985	TTP7DI3I6O
372188029554000	1	AIKWTIy9XU
501882545652000000	75.6999969	nN0YbZE1lM
3547081166710000	44.2999992	TTP7DI3I6O
3581426732340000	23.6000004	AIKWTIy9XU
3537606113520000	32.0999985	nN0YbZE1lM
3576591610880000	55.7999992	TTP7DI3I6O
6762938599990000	34.4000015	AIKWTIy9XU
3531212306530000	31.6000004	nN0YbZE1lM
4018273037990	12.3000002	TTP7DI3I6O
3540475349730000	89.9000015	AIKWTIy9XU
5610916558870000	39	nN0YbZE1lM
3579656804240000	44.7999992	TTP7DI3I6O
5602235583110000	70.9000015	AIKWTIy9XU
5169297944000000	6.69999981	nN0YbZE1lM
3582276111770000	40.2999992	TTP7DI3I6O
5108758011140000	59.0999985	AIKWTIy9XU
3541831798100000	5	nN0YbZE1lM
50381202053300000	58.2000008	TTP7DI3I6O
4911078331230000	24.8999996	AIKWTIy9XU
5412274866180000	2.29999995	nN0YbZE1lM
3588491988350000	61.2000008	TTP7DI3I6O
3574040775450000	61	AIKWTIy9XU
3533453465410000	63.7999992	nN0YbZE1lM
6759245831940000000	53	TTP7DI3I6O
502053582134000000	90	AIKWTIy9XU
58934059464100000	68.8000031	nN0YbZE1lM
3572034204810000	96.0999985	TTP7DI3I6O
374288831592000	3.0999999	AIKWTIy9XU
3536931350710000	74.3000031	nN0YbZE1lM
3553627592380000	26.3999996	TTP7DI3I6O
36514276000900	9.69999981	AIKWTIy9XU
6390146556390000	19.2000008	nN0YbZE1lM
3554846324360000	74.5999985	TTP7DI3I6O
5100131718200000	43.7000008	AIKWTIy9XU
67610319636100000	31	nN0YbZE1lM
3576287503780000	62.2999992	TTP7DI3I6O
201598868013000	80.8000031	AIKWTIy9XU
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
Fr7qf7vf	spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	Puce	http://dummyimage.com/222x246.jpg/5fa2dd/ffffff	Qmsjknuukmwayi	84	3545822686710000
7cis3t6g	N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	Puce	http://dummyimage.com/192x188.jpg/cc0000/ffffff	Ikwrejnzjjavpj	73	3535688087990000
Wtiwvymm	o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	Yellow	http://dummyimage.com/145x223.jpg/cc0000/ffffff	Abuccoitzplgzw	80	3542466010800000
23p7o1bz	0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	Purple	http://dummyimage.com/193x108.jpg/dddddd/000000	Jpynbxekpgpypi	10	3569338224050000
Czdzxm25	6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	Blue	http://dummyimage.com/244x137.jpg/cc0000/ffffff	Yliyegavsxzvyg	27	3586660333050000
G6t7cfla	t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	Pink	http://dummyimage.com/142x103.jpg/ff4444/ffffff	Dhkmwxiixoydjj	37	30365110892700
N2hqj4e9	Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	Aquamarine	http://dummyimage.com/100x172.jpg/cc0000/ffffff	Iqihmglwyegayz	4	4905652105280000000
K5wo1a43	f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	Purple	http://dummyimage.com/207x223.jpg/cc0000/ffffff	Jpzxkelfscdvdk	21	3565138551730000
O2ps6qpn	Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	Turquoise	http://dummyimage.com/216x144.jpg/ff4444/ffffff	Zkdqkwvoykowoe	72	5641821069610000
93dmli6s	nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	Maroon	http://dummyimage.com/165x129.jpg/5fa2dd/ffffff	Ugosigvveolqcx	49	5100177121940000
Wlqsakhk	jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	Maroon	http://dummyimage.com/106x144.jpg/ff4444/ffffff	Qysyoaaoedlhsy	57	3559383314010000
Ck75377k	QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	Orange	http://dummyimage.com/129x209.jpg/dddddd/000000	Gchiicdnyvfjfe	58	3573216731770000
Tg4dh7al	IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	Green	http://dummyimage.com/188x231.jpg/5fa2dd/ffffff	Sdddfmfaappjuf	26	633110105830000000
7zwnitv1	ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	Aquamarine	http://dummyimage.com/184x102.jpg/5fa2dd/ffffff	Ttfgnzdxbjzzvk	10	4905197192290000
132yb5a8	qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	Violet	http://dummyimage.com/130x236.jpg/dddddd/000000	Xlvpqgczwbhoqb	49	3582940702190000
0m8lqjpl	LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	Mauv	http://dummyimage.com/124x173.jpg/cc0000/ffffff	Djhlqeaprbesnl	39	5048372224310000
Gg5b4ghh	z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	Pink	http://dummyimage.com/214x137.jpg/cc0000/ffffff	Fntivjmunregph	63	67094986702100000
8vl7n5ss	vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	Green	http://dummyimage.com/161x125.jpg/dddddd/000000	Disucjskcmdfkx	47	3575871036560000
2zougqnq	Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	Goldenrod	http://dummyimage.com/148x210.jpg/ff4444/ffffff	Mmtkhswofmxxhn	61	6759263453300000000
F6pjumtr	I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	Red	http://dummyimage.com/164x160.jpg/cc0000/ffffff	Wfpmszyowrgafe	9	374622624522000
8suxbpqp	XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	Turquoise	http://dummyimage.com/221x102.jpg/ff4444/ffffff	Ekfsqixanhfiva	70	372301029043000
1vbjd4yz	fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	Pink	http://dummyimage.com/153x228.jpg/ff4444/ffffff	Dfphfbneyirjgd	12	3545822686710000
Mw12b01f	lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	Red	http://dummyimage.com/166x181.jpg/dddddd/000000	Gdusuqhxzumqzw	100	3535688087990000
1twrufia	OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	Yellow	http://dummyimage.com/130x200.jpg/dddddd/000000	Toyvjdsgvsckaz	95	3542466010800000
1n578ubp	PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	Turquoise	http://dummyimage.com/153x176.jpg/cc0000/ffffff	Wfqavlyprdnwji	34	3569338224050000
A25dkhme	spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	Aquamarine	http://dummyimage.com/142x201.jpg/cc0000/ffffff	Iyaipxkwkbpekg	99	3586660333050000
55krbfmi	N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	Violet	http://dummyimage.com/110x249.jpg/5fa2dd/ffffff	Misucsvzofwohd	49	30365110892700
Ojbp8dd0	o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	Violet	http://dummyimage.com/227x183.jpg/5fa2dd/ffffff	Iftskdxzlnwogq	30	4905652105280000000
1x8cbiu4	0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	Yellow	http://dummyimage.com/174x116.jpg/ff4444/ffffff	Afknmmavyeortk	30	3565138551730000
Rhps2fwh	6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	Goldenrod	http://dummyimage.com/117x247.jpg/ff4444/ffffff	Hnmbzzbkzonjdy	22	5641821069610000
Ksfx0a9j	t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	Red	http://dummyimage.com/227x146.jpg/5fa2dd/ffffff	Wfkpmtmkkmwnpa	79	5100177121940000
Nulbts7h	Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	Yellow	http://dummyimage.com/159x156.jpg/ff4444/ffffff	Xwwbmeauytrkkm	53	3559383314010000
Bsszsa2t	f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	Maroon	http://dummyimage.com/175x171.jpg/5fa2dd/ffffff	Xdmuorxspknudr	89	3573216731770000
3ll119tl	Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	Turquoise	http://dummyimage.com/166x209.jpg/dddddd/000000	Kqwvixovuutgwc	18	633110105830000000
Hwf6l7a4	nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	Yellow	http://dummyimage.com/242x207.jpg/dddddd/000000	Juugwrtzttwwqx	24	4905197192290000
Xdm9maaq	jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	Violet	http://dummyimage.com/108x179.jpg/5fa2dd/ffffff	Tvoupgqymvcdlc	96	3582940702190000
Halzx3xb	QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	Turquoise	http://dummyimage.com/231x181.jpg/ff4444/ffffff	Pujahjklkzgrrz	92	5048372224310000
N44hcf6g	IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	Teal	http://dummyimage.com/248x153.jpg/ff4444/ffffff	Vacalshggvhspg	70	67094986702100000
Wqamifbg	ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	Crimson	http://dummyimage.com/156x141.jpg/cc0000/ffffff	Bnnfillascjicn	86	3575871036560000
Zsipfkut	qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	Orange	http://dummyimage.com/168x128.jpg/dddddd/000000	Wqnepvjzrddgps	24	6759263453300000000
76fgdvuq	LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	Mauv	http://dummyimage.com/236x155.jpg/dddddd/000000	Trcvungecxuhmz	18	374622624522000
1yycniws	z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	Blue	http://dummyimage.com/103x136.jpg/ff4444/ffffff	Jnjuuwghgsdubc	16	372301029043000
Qbmzyq2b	vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	Crimson	http://dummyimage.com/184x208.jpg/cc0000/ffffff	Zefxjtjwcrumcb	79	3545822686710000
Gp5iq20s	Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	Yellow	http://dummyimage.com/126x159.jpg/ff4444/ffffff	Kbolrgmzmjoeai	30	3535688087990000
1fqdry3z	I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	Aquamarine	http://dummyimage.com/237x217.jpg/ff4444/ffffff	Jlevkdlgigbjit	42	3542466010800000
S0lwvzts	XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	Red	http://dummyimage.com/141x149.jpg/5fa2dd/ffffff	Hlebqgshfehpwq	29	3569338224050000
Prbo8k5o	fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	Crimson	http://dummyimage.com/194x206.jpg/5fa2dd/ffffff	Jtxkkdttcezxnv	72	3586660333050000
U5xz1i37	lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	Pink	http://dummyimage.com/248x213.jpg/ff4444/ffffff	Fsrctberptyurg	27	30365110892700
Ef31w8g5	OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	Orange	http://dummyimage.com/180x164.jpg/5fa2dd/ffffff	Gmjpzwwdtequvr	91	4905652105280000000
5k2h8bjs	PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	Mauv	http://dummyimage.com/180x249.jpg/dddddd/000000	Dvkyphrwahonrp	22	3565138551730000
Uowemmv9	spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	Pink	http://dummyimage.com/215x152.jpg/5fa2dd/ffffff	Zgbgghwvxsablu	14	5641821069610000
1a3d0qc7	N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	Mauv	http://dummyimage.com/118x239.jpg/ff4444/ffffff	Umjsjrhdarhzlr	33	5100177121940000
Mt5m689h	o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	Blue	http://dummyimage.com/130x233.jpg/cc0000/ffffff	Vyntpdrsgzoakb	77	3559383314010000
H0q35xg3	0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	Violet	http://dummyimage.com/244x137.jpg/dddddd/000000	Yujbiuihziswpi	24	3573216731770000
Sbzmk2ne	6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	Blue	http://dummyimage.com/135x240.jpg/ff4444/ffffff	Qbayopsclsbdxk	72	633110105830000000
Olgg8ikm	t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	Teal	http://dummyimage.com/115x203.jpg/5fa2dd/ffffff	Okfwcatnyyjcms	70	4905197192290000
7qy45q9m	Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	Mauv	http://dummyimage.com/154x211.jpg/dddddd/000000	Okkzspqcrnpzuc	5	3582940702190000
Fff389b7	f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	Violet	http://dummyimage.com/116x220.jpg/ff4444/ffffff	Iiewdgfnepkkln	13	5048372224310000
80pi0m1k	Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	Red	http://dummyimage.com/199x227.jpg/dddddd/000000	Vkcvvxwykrouah	8	67094986702100000
Leueiajp	nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	Violet	http://dummyimage.com/137x185.jpg/ff4444/ffffff	Xwnruceljxxwvt	48	3575871036560000
Szrokx16	jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	Fuscia	http://dummyimage.com/137x113.jpg/dddddd/000000	Zslmqwnjujbkwz	90	6759263453300000000
Dsjvz551	QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	Purple	http://dummyimage.com/186x176.jpg/ff4444/ffffff	Wpcuyxbzsmrhks	43	374622624522000
I83hky2i	IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	Crimson	http://dummyimage.com/159x186.jpg/dddddd/000000	Nffulkrjztqytd	75	372301029043000
Ugh3bb4d	ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	Khaki	http://dummyimage.com/222x173.jpg/5fa2dd/ffffff	Xoysexeymbisoz	40	3545822686710000
Qlk5swf9	qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	Green	http://dummyimage.com/102x234.jpg/ff4444/ffffff	Uqdpbplvymssbe	79	3535688087990000
3y24nwki	LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	Red	http://dummyimage.com/249x216.jpg/cc0000/ffffff	Qaivycixgxalzw	46	3542466010800000
312h7rn2	z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	Fuscia	http://dummyimage.com/151x214.jpg/cc0000/ffffff	Efghffqzdkghwg	24	3569338224050000
Zpuky8kf	vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	Crimson	http://dummyimage.com/148x134.jpg/dddddd/000000	Iciheyxilaeqsg	63	3586660333050000
32uvetb0	Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	Blue	http://dummyimage.com/102x155.jpg/dddddd/000000	Temizcldmsezkv	7	30365110892700
75xwsl1n	I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	Violet	http://dummyimage.com/140x106.jpg/cc0000/ffffff	Kktvgdtryxorsw	57	4905652105280000000
9mfvd1ru	XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	Purple	http://dummyimage.com/212x248.jpg/5fa2dd/ffffff	Bcimrwjuijkyuo	21	3565138551730000
Ncy3u6ke	fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	Orange	http://dummyimage.com/250x209.jpg/5fa2dd/ffffff	Nazypffkhbfbti	52	5641821069610000
Gvpvwl4l	lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	Blue	http://dummyimage.com/107x204.jpg/cc0000/ffffff	Hvckmkxvzqfpex	62	5100177121940000
Dg9kldz9	OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	Aquamarine	http://dummyimage.com/190x206.jpg/dddddd/000000	Vxmqcufazwnpoz	73	3559383314010000
Z70u7tte	PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	Aquamarine	http://dummyimage.com/179x134.jpg/5fa2dd/ffffff	Omplazkbclazbp	54	3573216731770000
H0d66fdl	spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	Green	http://dummyimage.com/104x236.jpg/ff4444/ffffff	Tfplneyzpzxklp	65	633110105830000000
Al74j0un	N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	Orange	http://dummyimage.com/209x174.jpg/cc0000/ffffff	Hgjrzwynqiegek	37	4905197192290000
J5wpkcym	o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	Purple	http://dummyimage.com/189x217.jpg/ff4444/ffffff	Aeziffogtwoful	62	3582940702190000
Iwze6f4x	0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	Aquamarine	http://dummyimage.com/194x231.jpg/5fa2dd/ffffff	Gddolwnmigxdkw	39	5048372224310000
B9gdbyiz	6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	Indigo	http://dummyimage.com/106x117.jpg/ff4444/ffffff	Mglyuvdtiylzsa	94	67094986702100000
Ovkdyhdt	t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	Violet	http://dummyimage.com/202x151.jpg/dddddd/000000	Gkoyhrfwkhsncv	97	3575871036560000
Qhvrlqej	Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	Red	http://dummyimage.com/148x108.jpg/ff4444/ffffff	Jdnspgcsdujesm	24	6759263453300000000
7i58b9qz	f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	Fuscia	http://dummyimage.com/239x118.jpg/cc0000/ffffff	Oipfdoiegwkdzo	33	374622624522000
J62886qq	Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	Orange	http://dummyimage.com/214x142.jpg/dddddd/000000	Qhozfwewmbatrt	99	372301029043000
Guqagxeu	nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	Violet	http://dummyimage.com/103x225.jpg/dddddd/000000	Sjyhwdgbswbgll	72	3545822686710000
0c3ko4bg	jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	Indigo	http://dummyimage.com/162x116.jpg/dddddd/000000	Smmavjflrnaqsz	38	3535688087990000
Tadnz4tb	QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	Blue	http://dummyimage.com/173x180.jpg/dddddd/000000	Wyvzcizrundbhz	1	3542466010800000
Uzaex4ax	IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	Puce	http://dummyimage.com/247x246.jpg/dddddd/000000	Lvkgmskvxpfbik	37	3569338224050000
5h1ywgjx	ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	Pink	http://dummyimage.com/151x167.jpg/ff4444/ffffff	Lcbmhftvfnovvr	61	3586660333050000
5uw67cst	qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	Green	http://dummyimage.com/217x234.jpg/cc0000/ffffff	Sggkosxwfexbcy	94	30365110892700
M9hwha1q	LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	Turquoise	http://dummyimage.com/118x199.jpg/ff4444/ffffff	Qwabnsbsayboft	52	4905652105280000000
Ltqn0spa	z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	Fuscia	http://dummyimage.com/231x167.jpg/dddddd/000000	Sgerxknrfurjuz	79	3565138551730000
Kld44346	vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	Green	http://dummyimage.com/114x159.jpg/dddddd/000000	Lfrncpfrgybcxo	43	5641821069610000
D049bs8s	Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	Crimson	http://dummyimage.com/153x238.jpg/dddddd/000000	Yhblfriquhmnsn	41	5100177121940000
Omfbvt7f	I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	Yellow	http://dummyimage.com/149x184.jpg/ff4444/ffffff	Syndnvvrxxmfqu	66	3559383314010000
L9lsuqlb	XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	Crimson	http://dummyimage.com/144x210.jpg/5fa2dd/ffffff	Jqthylahgdpsxv	55	3573216731770000
5aclorto	fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	Turquoise	http://dummyimage.com/248x222.jpg/ff4444/ffffff	Dsckyalehajiwa	48	633110105830000000
Ojptfgt0	lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	Red	http://dummyimage.com/243x189.jpg/cc0000/ffffff	Wrvrbflcotwdvl	62	4905197192290000
Srceqdxt	OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	Fuscia	http://dummyimage.com/127x232.jpg/dddddd/000000	Daglvsbqioqvpk	4	3582940702190000
Ei4rdfat	PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	Turquoise	http://dummyimage.com/127x212.jpg/ff4444/ffffff	Kuwhwruumuvher	40	5048372224310000
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.barang_dikembalikan (no_resi, no_urut, id_barang) FROM stdin;
815	1	Fr7qf7vf
141	2	7cis3t6g
15351	3	Wtiwvymm
1561	4	23p7o1bz
316	5	Czdzxm25
32511	6	G6t7cfla
212	7	N2hqj4e9
928	8	K5wo1a43
2396	9	O2ps6qpn
92962	10	93dmli6s
42264	11	Wlqsakhk
724	12	Ck75377k
3573	13	Tg4dh7al
8553	14	7zwnitv1
8353	15	132yb5a8
64535	16	0m8lqjpl
8435	17	Gg5b4ghh
8458	18	8vl7n5ss
964	19	2zougqnq
3577	20	F6pjumtr
816	21	8suxbpqp
142	22	1vbjd4yz
15352	23	Mw12b01f
1562	24	1twrufia
317	25	1n578ubp
32512	26	A25dkhme
213	27	55krbfmi
929	28	Ojbp8dd0
2397	29	1x8cbiu4
92963	30	Rhps2fwh
42265	31	Ksfx0a9j
725	32	Nulbts7h
3574	33	Bsszsa2t
8554	34	3ll119tl
8354	35	Hwf6l7a4
64536	36	Xdm9maaq
8436	37	Halzx3xb
8459	38	N44hcf6g
965	39	Wqamifbg
3578	40	Zsipfkut
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.barang_dikirim (no_resi, no_urut, id_barang, tanggal_review, review) FROM stdin;
815	1	Fr7qf7vf	2018-09-14	text/plain
141	2	7cis3t6g	2018-10-29	text/plain
15351	3	Wtiwvymm	2018-08-05	text/plain
1561	4	23p7o1bz	2018-07-22	text/plain
316	5	Czdzxm25	2019-02-27	text/plain
32511	6	G6t7cfla	2018-12-04	text/plain
212	7	N2hqj4e9	2019-03-17	text/plain
928	8	K5wo1a43	2018-10-16	text/plain
2396	9	O2ps6qpn	2018-10-15	text/plain
92962	10	93dmli6s	2019-02-22	text/plain
42264	11	Wlqsakhk	2019-01-30	text/plain
724	12	Ck75377k	2018-07-25	text/plain
3573	13	Tg4dh7al	2018-02-09	text/plain
8553	14	7zwnitv1	2018-01-12	text/plain
8353	15	132yb5a8	2018-07-16	text/plain
64535	16	0m8lqjpl	2018-10-25	text/plain
8435	17	Gg5b4ghh	2018-07-12	text/plain
8458	18	8vl7n5ss	2018-12-06	text/plain
964	19	2zougqnq	2018-09-13	text/plain
3577	20	F6pjumtr	2018-04-12	text/plain
724	21	8suxbpqp	2019-03-19	text/plain
3573	22	1vbjd4yz	2018-04-24	text/plain
8553	23	Mw12b01f	2018-02-12	text/plain
8353	24	1twrufia	2019-09-04	text/plain
64535	25	1n578ubp	2019-01-22	text/plain
8435	26	A25dkhme	2019-04-03	text/plain
8458	27	55krbfmi	2018-08-09	text/plain
964	28	Ojbp8dd0	2018-12-16	text/plain
3577	29	1x8cbiu4	2018-01-11	text/plain
8353	30	Rhps2fwh	2019-11-01	text/plain
64535	31	Ksfx0a9j	2019-02-03	text/plain
8435	32	Nulbts7h	2019-01-29	text/plain
8458	33	Bsszsa2t	2018-10-18	text/plain
964	34	3ll119tl	2019-01-03	text/plain
8353	35	Hwf6l7a4	2018-06-21	text/plain
64535	36	Xdm9maaq	2018-10-30	text/plain
8353	37	Halzx3xb	2018-12-15	text/plain
64535	38	N44hcf6g	2018-09-18	text/plain
8435	39	Wqamifbg	2019-02-23	text/plain
8458	40	Zsipfkut	2018-06-13	text/plain
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, nama_status, status) FROM stdin;
2830313305	1	Fr7qf7vf	2019-06-09	220	2020-01-15	Devshare	\N
704993243	2	7cis3t6g	2019-06-30	244	2020-02-29	Avaveo	\N
1669411268	3	Wtiwvymm	2019-07-29	66	2019-10-03	Skajo	\N
4113394080	4	23p7o1bz	2019-04-22	124	2019-08-24	Brainsphere	\N
3708100840	5	Czdzxm25	2019-05-29	157	2019-11-02	Snaptags	\N
9047169943	6	G6t7cfla	2019-07-27	230	2020-03-13	Chatterpoint	\N
7422123097	7	N2hqj4e9	2019-07-22	89	2019-10-19	Devshare	\N
786241503	8	K5wo1a43	2019-04-15	222	2019-11-23	Avaveo	\N
5957743325	9	O2ps6qpn	2019-06-13	242	2020-02-10	Skajo	\N
4234079690	10	93dmli6s	2019-04-22	306	2020-02-22	Brainsphere	\N
5744646100	11	Wlqsakhk	2019-06-27	43	2019-08-09	Snaptags	\N
1752073605	12	Ck75377k	2019-05-26	135	2019-10-08	Chatterpoint	\N
9158807215	13	Tg4dh7al	2019-07-11	66	2019-09-15	Devshare	\N
9641466717	14	7zwnitv1	2019-04-26	199	2019-11-11	Avaveo	\N
8188461563	15	132yb5a8	2019-06-27	127	2019-11-01	Skajo	\N
9748188880	16	0m8lqjpl	2019-05-17	226	2019-12-29	Brainsphere	\N
7994053538	17	Gg5b4ghh	2019-05-13	186	2019-11-15	Snaptags	\N
3056745187	18	8vl7n5ss	2019-04-20	259	2020-01-04	Chatterpoint	\N
6628427700	19	2zougqnq	2019-06-04	137	2019-10-19	Devshare	\N
5851343685	20	F6pjumtr	2019-05-16	90	2019-08-14	Avaveo	\N
1100282730	21	8suxbpqp	2019-06-10	239	2020-02-04	Skajo	\N
8785705774	22	1vbjd4yz	2019-05-08	268	2020-01-31	Brainsphere	\N
55373842	23	Mw12b01f	2019-04-22	253	2019-12-31	Snaptags	\N
3404241701	24	1twrufia	2019-05-25	300	2020-03-20	Chatterpoint	\N
5883081259	25	1n578ubp	2019-07-07	104	2019-10-19	Devshare	\N
9209749490	26	A25dkhme	2019-05-06	161	2019-10-14	Avaveo	\N
3088188814	27	55krbfmi	2019-07-12	221	2020-02-18	Skajo	\N
7117091442	28	Ojbp8dd0	2019-06-15	172	2019-12-04	Brainsphere	\N
2459779308	29	1x8cbiu4	2019-07-30	36	2019-09-04	Snaptags	\N
7671028175	30	Rhps2fwh	2019-06-07	214	2020-01-07	Chatterpoint	\N
9723348242	31	Ksfx0a9j	2019-07-24	152	2019-12-23	Devshare	\N
7651579499	32	Nulbts7h	2019-06-22	85	2019-09-15	Avaveo	\N
3682770744	33	Bsszsa2t	2019-04-13	154	2019-09-14	Skajo	\N
9531217737	34	3ll119tl	2019-04-15	125	2019-08-18	Brainsphere	\N
6889041286	35	Hwf6l7a4	2019-07-28	53	2019-09-19	Snaptags	\N
8164867765	36	Xdm9maaq	2019-06-08	112	2019-09-28	Chatterpoint	\N
394143030	37	Halzx3xb	2019-05-13	105	2019-08-26	Devshare	\N
5726466162	38	N44hcf6g	2019-07-22	247	2020-03-25	Avaveo	\N
8936287113	39	Wqamifbg	2019-05-17	304	2020-03-16	Skajo	\N
4367393406	40	Zsipfkut	2019-07-14	115	2019-11-06	Brainsphere	\N
1542912461	41	76fgdvuq	2019-04-28	210	2019-11-24	Snaptags	\N
1301395309	42	1yycniws	2019-04-29	270	2020-01-24	Chatterpoint	\N
4580800558	43	Qbmzyq2b	2019-07-28	151	2019-12-26	Devshare	\N
2331406599	44	Gp5iq20s	2019-05-30	221	2020-01-06	Avaveo	\N
4840376117	45	1fqdry3z	2019-05-04	205	2019-11-25	Skajo	\N
8820256472	46	S0lwvzts	2019-05-04	100	2019-08-12	Brainsphere	\N
2600787438	47	Prbo8k5o	2019-07-17	57	2019-09-12	Snaptags	\N
9161635061	48	U5xz1i37	2019-05-11	195	2019-11-22	Chatterpoint	\N
8664194993	49	Ef31w8g5	2019-07-08	74	2019-09-20	Devshare	\N
9508732498	50	5k2h8bjs	2019-06-19	169	2019-12-05	Avaveo	\N
2830313305	51	Uowemmv9	2019-06-14	105	2019-09-27	Skajo	\N
704993243	52	1a3d0qc7	2019-06-21	49	2019-08-09	Brainsphere	\N
1669411268	53	Mt5m689h	2019-05-08	87	2019-08-03	Snaptags	\N
4113394080	54	H0q35xg3	2019-07-03	276	2020-04-04	Chatterpoint	\N
3708100840	55	Sbzmk2ne	2019-04-28	105	2019-08-11	Devshare	\N
9047169943	56	Olgg8ikm	2019-06-20	233	2020-02-08	Avaveo	\N
7422123097	57	7qy45q9m	2019-05-19	308	2020-03-22	Skajo	\N
786241503	58	Fff389b7	2019-05-24	209	2019-12-19	Brainsphere	\N
5957743325	59	80pi0m1k	2019-05-30	119	2019-09-26	Snaptags	\N
4234079690	60	Leueiajp	2019-07-20	256	2020-04-01	Chatterpoint	\N
5744646100	61	Szrokx16	2019-04-29	201	2019-11-16	Devshare	\N
1752073605	62	Dsjvz551	2019-07-28	63	2019-09-29	Avaveo	\N
9158807215	63	I83hky2i	2019-04-21	126	2019-08-25	Skajo	\N
9641466717	64	Ugh3bb4d	2019-07-06	156	2019-12-09	Brainsphere	\N
8188461563	65	Qlk5swf9	2019-06-05	191	2019-12-13	Snaptags	\N
9748188880	66	3y24nwki	2019-05-12	178	2019-11-06	Chatterpoint	\N
7994053538	67	312h7rn2	2019-05-14	155	2019-10-16	Devshare	\N
3056745187	68	Zpuky8kf	2019-07-27	124	2019-11-28	Avaveo	\N
6628427700	69	32uvetb0	2019-05-20	257	2020-02-01	Skajo	\N
5851343685	70	75xwsl1n	2019-05-01	102	2019-08-11	Brainsphere	\N
1100282730	71	9mfvd1ru	2019-06-13	204	2020-01-03	Snaptags	\N
8785705774	72	Ncy3u6ke	2019-04-17	296	2020-02-07	Chatterpoint	\N
55373842	73	Gvpvwl4l	2019-06-06	287	2020-03-19	Devshare	\N
3404241701	74	Dg9kldz9	2019-05-28	238	2020-01-21	Avaveo	\N
5883081259	75	Z70u7tte	2019-04-17	117	2019-08-12	Skajo	\N
9209749490	76	H0d66fdl	2019-06-08	283	2020-03-17	Brainsphere	\N
3088188814	77	Al74j0un	2019-04-12	258	2019-12-26	Snaptags	\N
7117091442	78	J5wpkcym	2019-06-22	115	2019-10-15	Chatterpoint	\N
2459779308	79	Iwze6f4x	2019-05-11	164	2019-10-22	Devshare	\N
7671028175	80	B9gdbyiz	2019-06-19	226	2020-01-31	Avaveo	\N
9723348242	81	Ovkdyhdt	2019-06-03	136	2019-10-17	Skajo	\N
7651579499	82	Qhvrlqej	2019-07-29	24	2019-08-22	Brainsphere	\N
3682770744	83	7i58b9qz	2019-06-28	99	2019-10-05	Snaptags	\N
9531217737	84	J62886qq	2019-05-08	161	2019-10-16	Chatterpoint	\N
6889041286	85	Guqagxeu	2019-06-10	82	2019-08-31	Devshare	\N
8164867765	86	0c3ko4bg	2019-06-17	97	2019-09-22	Avaveo	\N
394143030	87	Tadnz4tb	2019-04-12	130	2019-08-20	Skajo	\N
5726466162	88	Uzaex4ax	2019-07-20	143	2019-12-10	Brainsphere	\N
8936287113	89	5h1ywgjx	2019-07-12	167	2019-12-26	Snaptags	\N
4367393406	90	5uw67cst	2019-06-24	168	2019-12-09	Chatterpoint	\N
1542912461	91	M9hwha1q	2019-07-10	125	2019-11-12	Devshare	\N
1301395309	92	Ltqn0spa	2019-06-27	48	2019-08-14	Avaveo	\N
4580800558	93	Kld44346	2019-05-01	117	2019-08-26	Skajo	\N
2331406599	94	D049bs8s	2019-06-12	187	2019-12-16	Brainsphere	\N
4840376117	95	Omfbvt7f	2019-06-23	178	2019-12-18	Snaptags	\N
8820256472	96	L9lsuqlb	2019-06-23	238	2020-02-16	Chatterpoint	\N
2600787438	97	5aclorto	2019-04-19	114	2019-08-11	Devshare	\N
9161635061	98	Ojptfgt0	2019-05-12	89	2019-08-09	Avaveo	\N
8664194993	99	Srceqdxt	2019-06-15	131	2019-10-24	Skajo	\N
9508732498	100	Ei4rdfat	2019-05-05	305	2020-03-05	Brainsphere	\N
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.chat (id, pesan, datetime, no_ktp_anggota, no_ktp_admin) FROM stdin;
1.0	image/jpeg	2018-10-16 00:00:00	3545822686710000	3545822686710000
2.0	application/powerpoint	2018-10-17 00:00:00	3535688087990000	3535688087990000
3.0	image/gif	2018-10-18 00:00:00	3542466010800000	3542466010800000
4.0	image/png	2018-10-19 00:00:00	3569338224050000	3569338224050000
5.0	video/quicktime	2018-10-20 00:00:00	3586660333050000	3586660333050000
6.0	application/pdf	2018-10-21 00:00:00	30365110892700	30365110892700
7.0	image/tiff	2018-10-22 00:00:00	4905652105280000000	4905652105280000000
8.0	image/png	2018-10-23 00:00:00	3565138551730000	3565138551730000
9.0	application/excel	2018-10-24 00:00:00	5641821069610000	5641821069610000
10.0	application/vnd.ms-powerpoint	2018-10-25 00:00:00	5100177121940000	5100177121940000
11.0	video/avi	2018-10-26 00:00:00	3559383314010000	3559383314010000
12.0	video/avi	2018-10-27 00:00:00	3573216731770000	3573216731770000
13.0	application/x-mspowerpoint	2018-10-28 00:00:00	633110105830000000	633110105830000000
14.0	image/png	2018-10-29 00:00:00	4905197192290000	4905197192290000
15.0	text/plain	2018-10-30 00:00:00	3582940702190000	3582940702190000
16.0	application/pdf	2018-10-31 00:00:00	5048372224310000	5048372224310000
17.0	video/mpeg	2018-11-01 00:00:00	67094986702100000	3545822686710000
18.0	application/x-troff-msvideo	2018-11-02 00:00:00	3575871036560000	3535688087990000
19.0	video/avi	2018-11-03 00:00:00	6759263453300000000	3542466010800000
20.0	video/x-mpeg	2018-11-04 00:00:00	374622624522000	3569338224050000
21.0	video/msvideo	2018-11-05 00:00:00	372301029043000	3586660333050000
22.0	application/vnd.ms-excel	2018-11-06 00:00:00	3545822686710000	30365110892700
23.0	video/x-msvideo	2018-11-07 00:00:00	3535688087990000	4905652105280000000
24.0	application/x-troff-msvideo	2018-11-08 00:00:00	3542466010800000	3565138551730000
25.0	video/x-msvideo	2018-11-09 00:00:00	3569338224050000	5641821069610000
26.0	image/png	2018-11-10 00:00:00	3586660333050000	5100177121940000
27.0	video/mpeg	2018-11-11 00:00:00	30365110892700	3559383314010000
28.0	audio/x-mpeg-3	2018-11-12 00:00:00	4905652105280000000	3573216731770000
29.0	audio/mpeg3	2018-11-13 00:00:00	3565138551730000	633110105830000000
30.0	video/msvideo	2018-11-14 00:00:00	5641821069610000	4905197192290000
31.0	application/powerpoint	2018-11-15 00:00:00	5100177121940000	3582940702190000
32.0	video/avi	2018-11-16 00:00:00	3559383314010000	5048372224310000
33.0	video/msvideo	2018-11-17 00:00:00	3573216731770000	3545822686710000
34.0	video/avi	2018-11-18 00:00:00	633110105830000000	3535688087990000
35.0	application/x-msexcel	2018-11-19 00:00:00	4905197192290000	3542466010800000
36.0	application/msword	2018-11-20 00:00:00	3582940702190000	3569338224050000
37.0	application/vnd.ms-powerpoint	2018-11-21 00:00:00	5048372224310000	3586660333050000
38.0	video/x-msvideo	2018-11-22 00:00:00	67094986702100000	30365110892700
39.0	image/pjpeg	2018-11-23 00:00:00	3575871036560000	4905652105280000000
40.0	application/msword	2018-11-24 00:00:00	6759263453300000000	3565138551730000
41.0	application/x-msexcel	2018-11-25 00:00:00	374622624522000	5641821069610000
42.0	video/x-msvideo	2018-11-26 00:00:00	372301029043000	5100177121940000
43.0	application/x-troff-msvideo	2018-11-27 00:00:00	3545822686710000	3559383314010000
44.0	application/pdf	2018-11-28 00:00:00	3535688087990000	3573216731770000
45.0	application/msword	2018-11-29 00:00:00	3542466010800000	633110105830000000
46.0	image/pjpeg	2018-11-30 00:00:00	3569338224050000	4905197192290000
47.0	application/x-mspowerpoint	2018-12-01 00:00:00	3586660333050000	3582940702190000
48.0	video/msvideo	2018-12-02 00:00:00	30365110892700	5048372224310000
49.0	application/msword	2018-12-03 00:00:00	4905652105280000000	3582940702190000
50.0	application/x-troff-msvideo	2018-12-04 00:00:00	3565138551730000	5048372224310000
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
Uowemmv9	AIKWTIy9XU	41594	37467
1a3d0qc7	AIKWTIy9XU	41026	6826
Mt5m689h	AIKWTIy9XU	99866	8225
H0q35xg3	AIKWTIy9XU	2668	15763
Sbzmk2ne	AIKWTIy9XU	65541	4776
Olgg8ikm	AIKWTIy9XU	8220	18949
7qy45q9m	AIKWTIy9XU	87626	20285
Fff389b7	AIKWTIy9XU	64885	4930
80pi0m1k	AIKWTIy9XU	1715	23806
Leueiajp	AIKWTIy9XU	90238	2317
Szrokx16	AIKWTIy9XU	37030	36477
Dsjvz551	AIKWTIy9XU	82629	25970
I83hky2i	AIKWTIy9XU	80413	5044
Ugh3bb4d	AIKWTIy9XU	41948	2058
Qlk5swf9	AIKWTIy9XU	91976	29196
3y24nwki	AIKWTIy9XU	6938	40632
312h7rn2	AIKWTIy9XU	99364	37913
Zpuky8kf	AIKWTIy9XU	81420	33843
32uvetb0	AIKWTIy9XU	80636	42502
75xwsl1n	AIKWTIy9XU	21200	9822
9mfvd1ru	AIKWTIy9XU	65515	6721
Ncy3u6ke	AIKWTIy9XU	25386	10947
Gvpvwl4l	AIKWTIy9XU	47444	15089
Dg9kldz9	AIKWTIy9XU	90754	30936
Z70u7tte	AIKWTIy9XU	53610	46472
H0d66fdl	AIKWTIy9XU	36828	47514
Al74j0un	AIKWTIy9XU	78810	18037
J5wpkcym	AIKWTIy9XU	11030	42091
Iwze6f4x	AIKWTIy9XU	80586	11766
B9gdbyiz	AIKWTIy9XU	57522	13678
Ovkdyhdt	AIKWTIy9XU	39062	43124
Qhvrlqej	AIKWTIy9XU	36624	19864
7i58b9qz	AIKWTIy9XU	62480	47378
J62886qq	AIKWTIy9XU	57677	15750
Guqagxeu	AIKWTIy9XU	54542	15990
0c3ko4bg	AIKWTIy9XU	29837	19604
Tadnz4tb	AIKWTIy9XU	29394	17033
Uzaex4ax	AIKWTIy9XU	16792	35121
5h1ywgjx	AIKWTIy9XU	19262	29593
5uw67cst	AIKWTIy9XU	9703	18506
M9hwha1q	AIKWTIy9XU	20863	23346
Ltqn0spa	AIKWTIy9XU	83751	24061
Xdm9maaq	nN0YbZE1lM	81363	8886
Halzx3xb	nN0YbZE1lM	47275	48851
N44hcf6g	nN0YbZE1lM	25442	19975
Wqamifbg	nN0YbZE1lM	14550	12502
Zsipfkut	nN0YbZE1lM	2057	11774
76fgdvuq	nN0YbZE1lM	56848	11512
1yycniws	nN0YbZE1lM	53031	44516
Qbmzyq2b	nN0YbZE1lM	57887	35851
Gp5iq20s	nN0YbZE1lM	29627	17580
1fqdry3z	nN0YbZE1lM	11427	35608
S0lwvzts	nN0YbZE1lM	89447	17812
Prbo8k5o	nN0YbZE1lM	90478	48367
U5xz1i37	nN0YbZE1lM	85527	11006
Ef31w8g5	nN0YbZE1lM	83765	38803
5k2h8bjs	nN0YbZE1lM	82856	10044
Uowemmv9	nN0YbZE1lM	34469	19670
1a3d0qc7	nN0YbZE1lM	52686	25747
Mt5m689h	nN0YbZE1lM	26	37480
H0q35xg3	nN0YbZE1lM	70309	47794
Sbzmk2ne	nN0YbZE1lM	9427	10942
Olgg8ikm	nN0YbZE1lM	54468	25566
7qy45q9m	nN0YbZE1lM	257	32353
Fff389b7	nN0YbZE1lM	70827	11214
80pi0m1k	nN0YbZE1lM	67749	25129
Leueiajp	nN0YbZE1lM	2764	46146
Szrokx16	nN0YbZE1lM	47015	25293
Dsjvz551	nN0YbZE1lM	41471	41661
I83hky2i	nN0YbZE1lM	23996	95
Ugh3bb4d	nN0YbZE1lM	97131	12833
Qlk5swf9	nN0YbZE1lM	19309	37429
3y24nwki	nN0YbZE1lM	450	3499
312h7rn2	nN0YbZE1lM	33270	8772
Zpuky8kf	nN0YbZE1lM	77953	30824
32uvetb0	nN0YbZE1lM	91838	3303
75xwsl1n	nN0YbZE1lM	7281	48274
9mfvd1ru	nN0YbZE1lM	57210	43852
Ncy3u6ke	nN0YbZE1lM	72026	44966
Gvpvwl4l	nN0YbZE1lM	38665	18602
Dg9kldz9	nN0YbZE1lM	71031	4330
Z70u7tte	nN0YbZE1lM	786	29627
H0d66fdl	nN0YbZE1lM	68490	12183
Al74j0un	nN0YbZE1lM	5057	30283
J5wpkcym	nN0YbZE1lM	37620	21758
Iwze6f4x	nN0YbZE1lM	45246	38580
B9gdbyiz	nN0YbZE1lM	8658	13479
Ovkdyhdt	nN0YbZE1lM	12453	1533
Qhvrlqej	nN0YbZE1lM	54044	22608
7i58b9qz	nN0YbZE1lM	35039	17663
J62886qq	nN0YbZE1lM	75032	38987
Guqagxeu	nN0YbZE1lM	10802	25886
0c3ko4bg	nN0YbZE1lM	57463	1790
Tadnz4tb	nN0YbZE1lM	98713	37136
Uzaex4ax	nN0YbZE1lM	30314	28127
5h1ywgjx	nN0YbZE1lM	21024	19390
5uw67cst	nN0YbZE1lM	98822	36221
M9hwha1q	nN0YbZE1lM	21198	38771
Ltqn0spa	nN0YbZE1lM	16738	19664
Kld44346	nN0YbZE1lM	73308	9598
D049bs8s	nN0YbZE1lM	78259	31711
Omfbvt7f	nN0YbZE1lM	23830	4645
L9lsuqlb	nN0YbZE1lM	31897	1245
5aclorto	nN0YbZE1lM	11980	19851
Ojptfgt0	nN0YbZE1lM	60759	12174
Srceqdxt	nN0YbZE1lM	35706	9005
Ei4rdfat	nN0YbZE1lM	51215	11980
Fr7qf7vf	TTP7DI3I6O	21801	49787
7cis3t6g	TTP7DI3I6O	54304	43584
Wtiwvymm	TTP7DI3I6O	13097	49479
23p7o1bz	TTP7DI3I6O	68570	22374
Czdzxm25	TTP7DI3I6O	96765	15610
G6t7cfla	TTP7DI3I6O	50458	29497
N2hqj4e9	TTP7DI3I6O	72740	19882
K5wo1a43	TTP7DI3I6O	11101	9368
O2ps6qpn	TTP7DI3I6O	44497	44016
93dmli6s	TTP7DI3I6O	46161	27127
Wlqsakhk	TTP7DI3I6O	78037	32010
Ck75377k	TTP7DI3I6O	56663	6330
Tg4dh7al	TTP7DI3I6O	47206	33042
7zwnitv1	TTP7DI3I6O	78426	3509
132yb5a8	TTP7DI3I6O	8325	1711
0m8lqjpl	TTP7DI3I6O	62531	35215
Gg5b4ghh	TTP7DI3I6O	55780	49246
8vl7n5ss	TTP7DI3I6O	99709	13522
2zougqnq	TTP7DI3I6O	35660	27832
F6pjumtr	TTP7DI3I6O	38103	43590
8suxbpqp	TTP7DI3I6O	18594	22274
1vbjd4yz	TTP7DI3I6O	4867	31471
Mw12b01f	TTP7DI3I6O	59706	2461
1twrufia	TTP7DI3I6O	34209	49925
1n578ubp	TTP7DI3I6O	4070	18878
A25dkhme	TTP7DI3I6O	13811	4934
55krbfmi	TTP7DI3I6O	28710	49033
Ojbp8dd0	TTP7DI3I6O	14228	21253
1x8cbiu4	TTP7DI3I6O	66020	18334
Rhps2fwh	TTP7DI3I6O	99324	39433
Ksfx0a9j	TTP7DI3I6O	53705	11780
Nulbts7h	TTP7DI3I6O	20611	22973
Bsszsa2t	TTP7DI3I6O	14202	16066
3ll119tl	TTP7DI3I6O	69482	18838
Hwf6l7a4	TTP7DI3I6O	16664	9119
Xdm9maaq	TTP7DI3I6O	98585	33342
Halzx3xb	TTP7DI3I6O	40302	29149
N44hcf6g	TTP7DI3I6O	23513	14324
Wqamifbg	TTP7DI3I6O	33528	39841
Zsipfkut	TTP7DI3I6O	42457	44659
76fgdvuq	TTP7DI3I6O	87334	26250
1yycniws	TTP7DI3I6O	33155	18844
Qbmzyq2b	TTP7DI3I6O	20720	16070
Gp5iq20s	TTP7DI3I6O	78057	29651
1fqdry3z	TTP7DI3I6O	20322	2695
S0lwvzts	TTP7DI3I6O	93629	36985
Prbo8k5o	TTP7DI3I6O	24591	22315
U5xz1i37	TTP7DI3I6O	54689	48372
Ef31w8g5	TTP7DI3I6O	56728	15039
5k2h8bjs	TTP7DI3I6O	34422	33386
Uowemmv9	TTP7DI3I6O	42025	32439
1a3d0qc7	TTP7DI3I6O	68115	22971
Mt5m689h	TTP7DI3I6O	97329	31145
H0q35xg3	TTP7DI3I6O	347	25010
Sbzmk2ne	TTP7DI3I6O	69854	55
Olgg8ikm	TTP7DI3I6O	6965	11644
7qy45q9m	TTP7DI3I6O	48445	20918
Fff389b7	TTP7DI3I6O	76188	24393
80pi0m1k	TTP7DI3I6O	26027	38103
Leueiajp	TTP7DI3I6O	72507	159
Szrokx16	TTP7DI3I6O	40805	9658
Dsjvz551	TTP7DI3I6O	8130	46868
I83hky2i	TTP7DI3I6O	74866	36430
Ugh3bb4d	TTP7DI3I6O	21368	46200
Qlk5swf9	TTP7DI3I6O	7800	47589
3y24nwki	TTP7DI3I6O	70615	13948
312h7rn2	TTP7DI3I6O	17708	7490
Zpuky8kf	TTP7DI3I6O	14283	24068
32uvetb0	TTP7DI3I6O	2934	12296
75xwsl1n	TTP7DI3I6O	51283	4105
9mfvd1ru	TTP7DI3I6O	49704	32728
Ncy3u6ke	TTP7DI3I6O	8996	49686
Gvpvwl4l	TTP7DI3I6O	48946	4009
Dg9kldz9	TTP7DI3I6O	22308	25396
Z70u7tte	TTP7DI3I6O	19782	46419
H0d66fdl	TTP7DI3I6O	48730	38306
Al74j0un	TTP7DI3I6O	77389	45516
J5wpkcym	TTP7DI3I6O	39889	2995
Iwze6f4x	TTP7DI3I6O	56270	15582
B9gdbyiz	TTP7DI3I6O	5708	29417
Ovkdyhdt	TTP7DI3I6O	50035	40111
Qhvrlqej	TTP7DI3I6O	73587	24415
7i58b9qz	TTP7DI3I6O	76219	38550
J62886qq	TTP7DI3I6O	53531	15071
Guqagxeu	TTP7DI3I6O	18886	27219
0c3ko4bg	TTP7DI3I6O	34006	2982
Tadnz4tb	TTP7DI3I6O	24640	10057
Uzaex4ax	TTP7DI3I6O	22837	492
5h1ywgjx	TTP7DI3I6O	17665	8690
5uw67cst	TTP7DI3I6O	71217	30705
M9hwha1q	TTP7DI3I6O	53383	39599
Ltqn0spa	TTP7DI3I6O	13381	22544
Kld44346	TTP7DI3I6O	61143	2072
D049bs8s	TTP7DI3I6O	79531	16140
Omfbvt7f	TTP7DI3I6O	69466	42182
L9lsuqlb	TTP7DI3I6O	63609	43883
5aclorto	TTP7DI3I6O	87033	10932
Ojptfgt0	TTP7DI3I6O	95205	22055
Srceqdxt	TTP7DI3I6O	30616	519
Ei4rdfat	TTP7DI3I6O	24591	9092
Fr7qf7vf	AIKWTIy9XU	23236	7488
7cis3t6g	AIKWTIy9XU	50962	11732
Wtiwvymm	AIKWTIy9XU	86109	29479
23p7o1bz	AIKWTIy9XU	21055	1151
Czdzxm25	AIKWTIy9XU	62477	2572
G6t7cfla	AIKWTIy9XU	84123	36621
N2hqj4e9	AIKWTIy9XU	12270	42046
K5wo1a43	AIKWTIy9XU	43600	26183
O2ps6qpn	AIKWTIy9XU	51911	11290
93dmli6s	AIKWTIy9XU	38071	35851
Wlqsakhk	AIKWTIy9XU	11939	43188
Ck75377k	AIKWTIy9XU	41145	17543
Tg4dh7al	AIKWTIy9XU	30439	11881
7zwnitv1	AIKWTIy9XU	43367	38448
132yb5a8	AIKWTIy9XU	13306	10139
0m8lqjpl	AIKWTIy9XU	3072	40960
Gg5b4ghh	AIKWTIy9XU	46330	29697
8vl7n5ss	AIKWTIy9XU	68112	47614
2zougqnq	AIKWTIy9XU	91552	38147
F6pjumtr	AIKWTIy9XU	94010	1230
8suxbpqp	AIKWTIy9XU	32590	33874
1vbjd4yz	AIKWTIy9XU	81372	21027
Mw12b01f	AIKWTIy9XU	45376	15874
1twrufia	AIKWTIy9XU	30172	35151
1n578ubp	AIKWTIy9XU	8798	21189
A25dkhme	AIKWTIy9XU	82992	11794
55krbfmi	AIKWTIy9XU	32195	43476
Ojbp8dd0	AIKWTIy9XU	62984	44767
1x8cbiu4	AIKWTIy9XU	69295	15268
Rhps2fwh	AIKWTIy9XU	28357	40531
Ksfx0a9j	AIKWTIy9XU	64602	3703
Nulbts7h	AIKWTIy9XU	46935	25141
Bsszsa2t	AIKWTIy9XU	30192	49258
3ll119tl	AIKWTIy9XU	26551	18432
Hwf6l7a4	AIKWTIy9XU	28772	17933
Xdm9maaq	AIKWTIy9XU	71132	4683
Halzx3xb	AIKWTIy9XU	80816	41165
N44hcf6g	AIKWTIy9XU	90346	10086
Wqamifbg	AIKWTIy9XU	82277	19211
Zsipfkut	AIKWTIy9XU	4569	32431
76fgdvuq	AIKWTIy9XU	90190	7453
1yycniws	AIKWTIy9XU	45543	403
Qbmzyq2b	AIKWTIy9XU	87969	24910
Gp5iq20s	AIKWTIy9XU	46001	43746
1fqdry3z	AIKWTIy9XU	50346	682
S0lwvzts	AIKWTIy9XU	91194	28395
Prbo8k5o	AIKWTIy9XU	42277	41931
U5xz1i37	AIKWTIy9XU	51285	35123
Ef31w8g5	AIKWTIy9XU	71211	15976
5k2h8bjs	AIKWTIy9XU	87180	8974
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	PEpXRzEKIC	74	5	7bjrkaTut0
N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	ZcQOn3qTp0	2	13	OhAgUgCCoU
o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	vF3GUTa5rw	91	97	vSLCic0Ghd
0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	tWNX2gwhve	27	65	obwOl5SBt4
6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	gi3t024xiJ	73	77	SDDC4AsUhx
t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	CKwHt2eQPg	13	37	LIR4Q37OzG
Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	4BvHfyw3EM	69	93	jSQD4nAKhO
f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	NEwxiSDlud	92	17	yvL1z5u9OS
Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	xgM45kMyJ6	7	9	C8DMzwYomF
nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	y53TMAfBw3	46	62	Pas4yPzcUG
jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	t7XEAizlmh	77	39	kBrohvDXe6
QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	DP2aALAqHx	91	13	5qvoFO1lCf
IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	cV36aRseBa	86	37	JTHk2j3MhR
ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	bELyyYTYtu	53	62	ldXBEJUf1B
qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	wHfYnS8GO1	41	4	IhUPHHZYQF
LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	VTZoEAGasM	92	97	7td0PWyiZG
z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	t3eXJuxEpS	19	52	aNnZCNXX2a
vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	Hopo24B4Zi	73	48	DPbcgEuZSa
Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	nhXBJLF6eE	80	88	IEIqKPGxth
I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	ryt2IIjERn	37	7	fYbt8BCqck
XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	Dwws5F7TvI	60	2	CvJNCPfEAp
fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	DKH8I20EIK	84	40	FaoRsdkVbo
lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	NLdMOAaPNN	93	46	j2ZyzTPrk7
OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	JqRx1QoCys	82	95	lY4EX6jL0v
PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	h2pELppFoe	64	64	BkHOE1ckWl
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.kategori (nama, level, sub_dari) FROM stdin;
motorik	1	\N
nonmotorik	1	\N
sepeda	2	\N
bola basket	2	motorik
catur	2	nonmotorik
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.kategori_item (nama_item, nama_kategori) FROM stdin;
spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	motorik
N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	bola basket
o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	sepeda
0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	nonmotorik
6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	catur
t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	nonmotorik
Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	motorik
f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	sepeda
Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	catur
nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	sepeda
jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	catur
QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	nonmotorik
IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	bola basket
ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	bola basket
qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	sepeda
LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	nonmotorik
z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	nonmotorik
vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	nonmotorik
Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	nonmotorik
I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	motorik
XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	nonmotorik
fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	nonmotorik
lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	catur
OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	nonmotorik
PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	motorik
spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	catur
N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	sepeda
o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	motorik
0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	motorik
6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	motorik
t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	catur
Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	catur
f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	nonmotorik
Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	motorik
nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	catur
jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	bola basket
QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	bola basket
IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	sepeda
ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	nonmotorik
qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	motorik
LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	sepeda
z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	sepeda
vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	sepeda
Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	catur
I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	nonmotorik
XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	catur
0AifvT4W8ROtNh4Ste2KkUOAtRBQshfAIRP3izhHAVxntBEnHQ	catur
6vJPgkRsMYIbyIwvZtHADD0WqW1eZJVGgyacNmALQDS2fMmnc0	sepeda
Bzqfr24atTbVrbltINwUXbm2WuxyXuPOf4D7HCkLrTumtdDtQF	sepeda
f4LY3O03qrPWlQzJa6YtqN1knZTpF5gXhqg05H5iDKg9pMq9Nu	motorik
fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	bola basket
fEYXVRNDNn7U21DuLV7CgnOTPLax8w9qlYQsThuqFr58Er3SKS	catur
I3NMR0n4cNcNHcKQhRD0Z9UJ5ocjZEvsOFZdy64OTjmka57qg4	catur
IJNgZ2oXFZSiI95uwfsGE1bse9ARNvRsqiGtNevICusfzXhvtV	motorik
jinnXFCfwMWacZsJf2EQFAtCAzSo5vfr3rWVpima8QHPRy7SLp	nonmotorik
Lg09031VzIOd5j6haNBQQ9b3fvF8Mu9hSqdIx3EHCEslPkT1g8	bola basket
lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	bola basket
lQ6zrQPhRInpsyinQY2OZk0qleTx8Zsz0Zj0CTKjzk2svZrzsF	nonmotorik
LV58B50EioHFDut2vG8lBKOfQy92HyKRzr5KcpQDPKSv4CekgP	catur
N0flngzQeGLM531gA1eeNUKL5weqB6Gp2Y0y1qMhV2hn1ufq0D	nonmotorik
nlmDqAJ3rDhS7sCl8duS3nGlQwigoQVmdr2kR23LpPPBiuAVKN	nonmotorik
o6IUwhULRy07hoZnv7IiAVgNn9mVa8nyYExTGSGI9ejMSRAVGq	catur
OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	motorik
OSZzsH4MweE2MgWnKiaxF7nWA9S7kYkAfP7KxYSSUBabYqp2cL	sepeda
PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	catur
PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	sepeda
qOGiWo0UYMeXhxV8WNAdXXP9TfOxL01PQZY7sQBoiYWUIO6jpG	nonmotorik
QoRy3dvM2AcQBiUzrxfD6EokqFimQMPxKwqswMoW0Zpv6TAJUs	sepeda
spXB4RpwmhNesATZMjvte2lM21hmzL7g3SqipWW4O2OEXmXaam	bola basket
t5uAv70duNCjmwMx7hhaD0beuOzdSm5ISty780H66FYgtsaQzG	sepeda
ujDTJ52GbPi2T5zuIyAscVQpkYYPf81gxKlT2GIwm48myQ9LuH	motorik
vbMcMmqGSpFsX2ccGtrOh0aSDo2LDDOmLTHDKqhKXiAgbszu7k	motorik
XzhzXts9qwDeW79V7Q4upR1OEOIr75nzcHb4VSvzVR6kBABeum	motorik
z5V11dkNUZW1d11Lm5EC7EIRMiREwHw7zPdeSMJmze1glFQm9E	bola basket
Zld8c5uByioLjvVL96drllyj3POycgSkR5uLnvaigNrjt9jgiF	sepeda
PIcEgL8UTafZP3trYqgvjkdXZChUStTQVXbIVwtcEeYYcLJAUq	bola basket
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
nN0YbZE1lM	328983040	pjF1IQcjo74xu9DquFML
TTP7DI3I6O	316556704	4AQaQggxsnIpSDocVXGf
AIKWTIy9XU	622338496	oa2tsnaNjYdpKjoAebwj
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
2830313305	2019-02-16 00:00:00	25	51.7599983	78	3545822686710000	Devshare
704993243	2018-09-10 00:00:00	6	90.6399994	31.1000004	3535688087990000	Avaveo
1669411268	2018-05-20 00:00:00	19	25.8799992	84	3542466010800000	Skajo
4113394080	2019-03-13 00:00:00	43	40.1699982	16.6000004	3569338224050000	Brainsphere
3708100840	2018-11-02 00:00:00	49	32.9300003	12.1000004	3586660333050000	Snaptags
9047169943	2018-09-15 00:00:00	97	58.8300018	1.39999998	30365110892700	Chatterpoint
7422123097	2018-12-19 00:00:00	32	24.5300007	22.6000004	4905652105280000000	Devshare
786241503	2018-04-10 00:00:00	20	85.5599976	62.0999985	3565138551730000	Avaveo
5957743325	2019-03-23 00:00:00	85	20.6499996	10	5641821069610000	Skajo
4234079690	2018-05-03 00:00:00	29	92.4700012	7.5	5100177121940000	Brainsphere
5744646100	2019-02-12 00:00:00	95	60.1100006	15.5	3559383314010000	Snaptags
1752073605	2018-06-04 00:00:00	17	40.6599998	51.5999985	3573216731770000	Chatterpoint
9158807215	2018-11-16 00:00:00	93	86.2699966	19	633110105830000000	Devshare
9641466717	2019-02-11 00:00:00	60	83.9499969	49.0999985	4905197192290000	Avaveo
8188461563	2018-06-28 00:00:00	37	74.8899994	98.5	3582940702190000	Devshare
9748188880	2018-07-16 00:00:00	23	41.5200005	27.7999992	5048372224310000	Avaveo
7994053538	2019-03-23 00:00:00	63	76.6699982	51.9000015	67094986702100000	Skajo
3056745187	2018-10-22 00:00:00	82	22.8999996	83.6999969	3575871036560000	Brainsphere
6628427700	2018-11-24 00:00:00	97	50.0900002	89	6759263453300000000	Snaptags
5851343685	2019-03-15 00:00:00	7	46.25	73.5999985	374622624522000	Devshare
1100282730	2018-06-03 00:00:00	31	77.3899994	83.8000031	372301029043000	Avaveo
8785705774	2018-05-12 00:00:00	53	41.2999992	7.5	5602252082080000	Skajo
55373842	2018-05-09 00:00:00	100	95.2399979	61.9000015	5602234304180000	Brainsphere
3404241701	2018-06-08 00:00:00	88	94.9300003	38.0999985	348159958884000	Snaptags
5883081259	2018-10-31 00:00:00	96	99.0400009	17	374288379788000	Chatterpoint
9209749490	2019-04-07 00:00:00	89	33.4199982	7.5	5387749970420000	Snaptags
3088188814	2018-06-12 00:00:00	92	98.1299973	29.8999996	3569441237400000	Devshare
7117091442	2018-05-25 00:00:00	41	60.3100014	39.5	502043683482000000	Avaveo
2459779308	2018-12-29 00:00:00	50	5.26000023	84.1999969	30436099908300	Skajo
7671028175	2019-01-05 00:00:00	86	31.8199997	32.5	6304943803570000	Brainsphere
9723348242	2019-01-03 00:00:00	5	94.8700027	69.0999985	5610443074650000	Snaptags
7651579499	2019-04-01 00:00:00	84	49.8899994	44.0999985	374283591109000	Chatterpoint
3682770744	2018-12-17 00:00:00	59	80.9800034	64.3000031	3543179044810000	Devshare
9531217737	2018-06-05 00:00:00	32	19.3500004	13.6000004	5602254999890000	Avaveo
6889041286	2018-04-21 00:00:00	91	46.25	90.8000031	3543016151220000	Skajo
8164867765	2019-03-23 00:00:00	32	15.1000004	17.2000008	5602245175720000	Brainsphere
394143030	2019-01-28 00:00:00	75	50.0400009	20.6000004	5137738202050000	Snaptags
5726466162	2018-06-11 00:00:00	58	73.5	53.7999992	3581920451130000	Chatterpoint
8936287113	2018-08-06 00:00:00	42	38.5200005	78	560222560864000000	Devshare
4367393406	2018-09-01 00:00:00	43	30.6599998	43.2000008	3588286776860000	Avaveo
1542912461	2018-07-13 00:00:00	83	44.2099991	64	4844494273670000	Skajo
1301395309	2018-06-15 00:00:00	24	25.7800007	23	3565420812250000	Brainsphere
4580800558	2018-07-22 00:00:00	59	42.0200005	14.6999998	677197306217000000	Snaptags
2331406599	2019-03-12 00:00:00	99	77.1600037	79.5999985	3580900625390000	Devshare
4840376117	2019-03-18 00:00:00	37	40.9000015	28.2000008	3588497997660000	Avaveo
8820256472	2018-07-28 00:00:00	22	49.0999985	24.3999996	501842370868000000	Skajo
2600787438	2018-04-13 00:00:00	44	36.4300003	95.8000031	3587363223840000	Brainsphere
9161635061	2018-10-23 00:00:00	23	73.9700012	94	30169099037500	Devshare
8664194993	2019-03-09 00:00:00	69	75.75	29.8999996	201953385226000	Avaveo
9508732498	2018-09-24 00:00:00	57	26.9599991	20.2999992	676709294748000000	Skajo
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
815	2830313305	9zrl8WkR8JHOYMg	31575	9378-06-01	3545822686710000	Vkrmymqjxfppgyupjvgaw
141	704993243	kcSAI8COmvi1sze	34463	3182-02-15	3535688087990000	Itkjlfdiqlsqsheektqyu
15351	1669411268	zteCi8EOHzaZVI2	44381	7205-11-20	3542466010800000	Wvucjatdotughtjydifxh
1561	4113394080	L8uLeH4ysduYIzL	18860	9704-08-21	3569338224050000	Yqqblgznmgzlztljiyjtr
316	3708100840	TAi2yfotq6gmX42	19631	7962-10-18	3586660333050000	Rbrftkmhrznbrhajxbrxh
32511	9047169943	CX1c0pegNwEv4cM	15231	5804-03-15	30365110892700	Uojxnotidqtnlztcvkapy
212	7422123097	iwuOnr51ZujZK9v	36716	5717-02-20	4905652105280000000	Lnbquklekmoqsgdwkogxi
928	786241503	IlitAviNpeNR5UL	43761	9751-05-12	3565138551730000	Ksmkxjzribzsoevahouex
2396	5957743325	eDHqsIEeJQjk5ds	3358	3791-02-17	5641821069610000	Trizlcebdclvbostonwhj
92962	4234079690	ySx82v4lliYg56c	16827	5142-10-12	5100177121940000	Ozqbgrshvzhibocjocikk
42264	5744646100	NhTJlWR1RZIRu9e	8165	3015-11-15	3559383314010000	Btcbhdifdbgkajphdslhm
724	1752073605	T4OXYMmqN1TWlYQ	37487	6343-05-03	3573216731770000	Lpiinbfirgihmcmpimseg
3573	9158807215	jKu3opz15PjQQ9g	4160	3921-07-17	633110105830000000	Qreafdsqmixyfciteymyx
8553	9641466717	CbwUPUpbp6uyC4I	42167	2885-10-05	4905197192290000	Qblqdbapbrqrjuhjmnnhv
8353	8188461563	mqYzuteRbABdYBE	35649	3470-09-27	3582940702190000	Tjgxugjqccmusboixckpg
64535	9748188880	qiFxd2MhBizuhse	26392	8397-01-20	5048372224310000	Zqhocdokjgvcuankiglcx
8435	7994053538	GixUHnqyWXVtsnU	17066	7064-02-19	67094986702100000	Akmvzukaooguxytmillyf
8458	3056745187	nONZZnkgoltUWyS	12659	8432-03-20	3575871036560000	Rwdqvmvvxdnwlojyyxvhr
964	6628427700	DdbTQ29BsyLWsX5	16417	6257-04-17	6759263453300000000	Iodmrcdphdisgswbqgylz
3577	5851343685	EUpT0zkev5mMZCD	26013	5021-05-12	374622624522000	Hoxhyfhlletwkyjvlntke
816	1100282730	9zrl8WkR8JHOYMg	31575	9378-06-01	372301029043000	Vywzgcktlpzcerlabrydx
142	8785705774	kcSAI8COmvi1sze	34463	3182-02-15	3545822686710000	Zqbirugexrmchfyqxryot
15352	55373842	zteCi8EOHzaZVI2	44381	7205-11-20	3535688087990000	Obldpcslwtdgxlruppuah
1562	3404241701	L8uLeH4ysduYIzL	18860	9704-08-21	3542466010800000	Bkndbrqmydceecsmqdgqf
317	5883081259	TAi2yfotq6gmX42	19631	7962-10-18	3569338224050000	Cqjyowwxlehfdjuolhfmm
32512	9209749490	CX1c0pegNwEv4cM	15231	5804-03-15	3586660333050000	Rjcnbkchiwcstvpeljdpd
213	3088188814	iwuOnr51ZujZK9v	36716	5717-02-20	30365110892700	Hqzovhwfzmsjiiejrfirf
929	7117091442	IlitAviNpeNR5UL	43761	9751-05-12	4905652105280000000	Cgbguzrqmjzvdcicefheq
2397	2459779308	eDHqsIEeJQjk5ds	3358	3791-02-17	3565138551730000	Ylzanqqrzlnlfenckocpp
92963	7671028175	ySx82v4lliYg56c	16827	5142-10-12	5641821069610000	Yqbmdcaliwpuoyfkadidt
42265	9723348242	NhTJlWR1RZIRu9e	8165	3015-11-15	5100177121940000	Itxargudltlziionxukzc
725	7651579499	T4OXYMmqN1TWlYQ	37487	6343-05-03	3559383314010000	Hootbweruadrioczueuyv
3574	3682770744	jKu3opz15PjQQ9g	4160	3921-07-17	3573216731770000	Xwwugkjtgiawguckcuhyj
8554	9531217737	CbwUPUpbp6uyC4I	42167	2885-10-05	633110105830000000	Eitpqbqwvbkpcdxlgpgyg
8354	6889041286	mqYzuteRbABdYBE	35649	3470-09-27	4905197192290000	Yphrrgiohxlzizfptwait
64536	8164867765	qiFxd2MhBizuhse	26392	8397-01-20	3582940702190000	Rftopdogvjehznvjybgha
8436	394143030	GixUHnqyWXVtsnU	17066	7064-02-19	5048372224310000	Cktsnzocjrjownchmpvly
8459	5726466162	nONZZnkgoltUWyS	12659	8432-03-20	67094986702100000	Nkchivuyuocghwvothxxx
965	8936287113	DdbTQ29BsyLWsX5	16417	6257-04-17	3575871036560000	Lchoaqnzleygfnjkpnurf
3578	4367393406	EUpT0zkev5mMZCD	26013	5021-05-12	6759263453300000000	Azpfgxskyddjrihurhquh
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
3545822686710000	Kellie Bertot	kbertot0@addtoany.com	2002-01-20	972-777-0293
3535688087990000	Mirabella Mulbery	mmulbery1@skyrock.com	2013-03-14	992-631-1165
3542466010800000	Jack Surman	jsurman2@patch.com	2013-09-25	461-583-0637
3569338224050000	Tod Borgars	tborgars3@google.pl	2010-05-31	333-588-0299
3586660333050000	Anthe Slocket	aslocket4@tripod.com	2009-02-21	578-507-5607
30365110892700	Anjela Shewan	ashewan5@opera.com	2010-09-06	259-662-2954
4905652105280000000	Cloe Adamovsky	cadamovsky6@delicious.com	2006-10-01	454-879-7338
3565138551730000	Garrik Matas	gmatas7@oracle.com	2008-11-03	898-639-5181
5641821069610000	Fredia Batters	fbatters8@printfriendly.com	2003-03-14	113-245-3299
5100177121940000	Charlene Haversham	chaversham9@ucoz.ru	2001-05-05	763-361-5510
3559383314010000	Bartel De Bellis	bdea@hubpages.com	2011-05-13	858-272-1097
3573216731770000	Griselda Beeden	gbeedenb@cloudflare.com	2010-04-14	128-735-9246
633110105830000000	Clara Petrou	cpetrouc@google.co.jp	2006-01-20	743-484-2374
4905197192290000	Harri Taunton	htauntond@nbcnews.com	2008-07-29	163-413-6181
3582940702190000	Maxim Hamel	mhamele@alibaba.com	2006-07-05	809-277-8325
5048372224310000	Meghan Wallentin	mwallentinf@domainmarket.com	2001-11-26	379-722-9314
67094986702100000	Leena Baldoni	lbaldonig@yale.edu	2001-04-16	733-537-1585
3575871036560000	Teodor Vaudrey	tvaudreyh@topsy.com	2009-09-16	126-331-3784
6759263453300000000	Hamlin Jopson	hjopsoni@abc.net.au	2005-11-18	441-659-3708
374622624522000	Klaus Sinton	ksintonj@nifty.com	2014-06-19	366-466-0173
372301029043000	Wandis Ellershaw	wellershawk@twitpic.com	2014-04-02	657-637-8522
5602252082080000	Riva Cownden	rcowndenl@sfgate.com	2011-04-30	560-785-6268
5602234304180000	Creigh McShane	cmcshanem@histats.com	2010-05-08	931-489-1295
348159958884000	Teddy Seleway	tselewayn@flickr.com	1999-07-04	274-507-6447
374288379788000	Lind Quirke	lquirkeo@google.com.hk	1999-07-12	353-119-8800
5387749970420000	Bealle McGrouther	bmcgroutherp@noaa.gov	2006-05-22	314-256-4120
3569441237400000	Opaline Immings	oimmingsq@fda.gov	2008-02-05	491-671-7189
502043683482000000	Pascal Orry	porryr@plala.or.jp	2013-12-02	304-930-2422
30436099908300	Dev Betonia	dbetonias@last.fm	2012-08-04	683-514-1903
6304943803570000	Ellsworth Clarae	eclaraet@cpanel.net	2014-11-26	397-189-5015
5610443074650000	Demott Walworth	dwalworthu@icio.us	2002-02-17	468-337-2661
374283591109000	Fiona Canepe	fcanepev@so-net.ne.jp	2001-01-22	535-543-8189
3543179044810000	Billi Conneely	bconneelyw@moonfruit.com	2008-05-07	559-758-3104
5602254999890000	Gordie Bridge	gbridgex@cbsnews.com	2008-11-30	124-475-3466
3543016151220000	Ab Hutcheon	ahutcheony@example.com	2004-06-23	939-796-7564
5602245175720000	Neal Sowray	nsowrayz@engadget.com	2005-03-17	310-925-5522
5137738202050000	Murielle Prevett	mprevett10@hexun.com	1999-04-22	355-823-6452
3581920451130000	Farah Retallack	fretallack11@oracle.com	2007-11-13	492-752-0569
560222560864000000	Alma Blinkhorn	ablinkhorn12@issuu.com	2001-06-11	285-129-6532
3588286776860000	Cara Mc Ilwrick	cmc13@surveymonkey.com	2008-05-20	356-421-2805
4844494273670000	Adena Mangeot	amangeot14@nyu.edu	2012-05-12	625-927-8701
3565420812250000	Clarance Peepall	cpeepall15@freewebs.com	2003-12-28	503-935-6881
677197306217000000	Demeter Pull	dpull16@springer.com	2007-10-18	387-796-8587
3580900625390000	Winifield Stampfer	wstampfer17@123-reg.co.uk	2002-11-09	451-875-0007
3588497997660000	Amaleta Klimecki	aklimecki18@ameblo.jp	2001-06-04	916-809-9736
501842370868000000	Jerrilee Peggs	jpeggs19@addthis.com	2003-04-11	807-523-6597
3587363223840000	Ramon Babonau	rbabonau1a@desdev.cn	2013-07-07	187-431-7804
30169099037500	Jessee Jelliman	jjelliman1b@360.cn	2007-05-03	641-488-2508
201953385226000	Angelique Murrell	amurrell1c@amazon.co.jp	2012-10-21	221-402-3802
676709294748000000	Vanessa McShane	vmcshane1d@cafepress.com	2006-03-11	169-991-6983
6709316972100000	Syman Wileman	swileman1e@craigslist.org	2014-01-10	793-838-3124
633483832358000000	Olympie Cleatherow	ocleatherow1f@yelp.com	2001-05-04	229-212-0357
3585923020210000	Ardenia Treadgall	atreadgall1g@discovery.com	1999-05-25	322-935-5056
372188029554000	Irvine Plumridge	iplumridge1h@thetimes.co.uk	2004-09-28	643-730-9308
501882545652000000	Tabby Sauvain	tsauvain1i@unc.edu	2002-09-07	639-990-6919
3547081166710000	Nevins Pitkeathly	npitkeathly1j@twitter.com	1999-09-25	980-231-1199
3581426732340000	Valina Watson-Brown	vwatsonbrown1k@tripadvisor.com	2011-12-16	719-308-6570
3537606113520000	Wyn Runcie	wruncie1l@cornell.edu	2011-01-18	617-208-1383
3576591610880000	Bobinette McAughtrie	bmcaughtrie1m@omniture.com	2006-09-04	871-509-3870
6762938599990000	Marys Rodmell	mrodmell1n@house.gov	2006-03-01	323-194-2631
3531212306530000	Mariele Nutbeam	mnutbeam1o@ustream.tv	1999-05-09	648-788-1681
4018273037990	Viva Alves	valves1p@4shared.com	2008-01-04	965-592-7481
3540475349730000	Maggee Robbeke	mrobbeke1q@51.la	2013-02-27	592-955-9937
5610916558870000	Steffie Elgie	selgie1r@dmoz.org	2004-01-29	906-441-9299
3579656804240000	Justine Salzburg	jsalzburg1s@macromedia.com	2009-06-09	875-145-3483
5602235583110000	Geordie Wison	gwison1t@lulu.com	2003-05-17	201-237-0128
5169297944000000	Tobe Kealy	tkealy1u@newsvine.com	2015-02-10	148-813-7829
3582276111770000	Magda Drinkhall	mdrinkhall1v@dell.com	2013-12-11	743-577-4167
5108758011140000	Esdras Watters	ewatters1w@amazon.co.jp	2013-03-04	934-420-6144
3541831798100000	Marietta Postlewhite	mpostlewhite1x@aboutads.info	2010-01-17	332-663-9504
50381202053300000	Darby Twinborough	dtwinborough1y@princeton.edu	2002-07-31	181-841-9547
4911078331230000	Israel Cutbush	icutbush1z@zdnet.com	2014-01-04	328-429-4972
5412274866180000	Barnabe Dumpleton	bdumpleton20@reference.com	2002-01-26	692-997-4298
3588491988350000	Darya Muncey	dmuncey21@ftc.gov	2005-10-22	810-883-2120
3574040775450000	Tadeo Showers	tshowers22@sphinn.com	2002-03-15	631-101-3476
3533453465410000	Shep Deniseau	sdeniseau23@goo.gl	2012-01-14	864-452-5976
6759245831940000000	Rosana Pudney	rpudney24@deliciousdays.com	2001-12-04	624-358-7677
502053582134000000	Coretta Nilles	cnilles25@ucla.edu	2013-07-14	206-744-7032
58934059464100000	Nari Gouthier	ngouthier26@auda.org.au	2001-06-10	275-497-4381
3572034204810000	Giles Marns	gmarns27@ebay.com	2013-04-28	916-950-1561
374288831592000	Ainslee Beverstock	abeverstock28@netscape.com	2001-01-11	121-882-7233
3536931350710000	Melisse De Few	mde29@unesco.org	2000-08-04	764-580-0558
3553627592380000	Henrik Winterflood	hwinterflood2a@slate.com	2014-05-17	260-757-6346
36514276000900	Martelle Aspden	maspden2b@oracle.com	2012-08-05	820-717-7427
6390146556390000	Ettie Cluckie	ecluckie2c@elegantthemes.com	2002-03-03	904-172-4148
3554846324360000	Fredek Rennocks	frennocks2d@chronoengine.com	2013-04-02	714-267-9799
5100131718200000	Ronny Barthelet	rbarthelet2e@usatoday.com	2012-08-26	678-329-4949
67610319636100000	Carin Challener	cchallener2f@feedburner.com	2003-03-25	160-214-3510
3576287503780000	Blair Brasse	bbrasse2g@scribd.com	2000-07-25	128-879-1732
201598868013000	Skylar Jozefczak	sjozefczak2h@disqus.com	2010-08-23	598-287-8716
201523304609000	Alvira Aylen	aaylen2i@cloudflare.com	1999-04-28	317-240-9626
6384240135300000	Dallon Gherardesci	dgherardesci2j@adobe.com	2010-08-18	260-477-9536
5410549446220000	Austine Solomonides	asolomonides2k@umich.edu	2009-02-04	947-787-9453
371798556036000	Irina Cosin	icosin2l@home.pl	2003-09-25	690-128-1532
63049993342000000	Calida Muddicliffe	cmuddicliffe2m@bloglovin.com	2001-08-07	918-234-8326
3568208106680000	Amabel Reape	areape2n@jimdo.com	1999-10-10	245-289-2584
3535253535130000	Brandon Kinkaid	bkinkaid2o@devhub.com	2002-05-16	591-946-7649
3545531473620000	Winnie Float	wfloat2p@sitemeter.com	1999-08-31	724-200-4472
30208250021800	Ebba Guitt	eguitt2q@abc.net.au	1999-09-12	668-672-9094
5893383387320000	Stormi Stonhewer	sstonhewer2r@amazonaws.com	2014-12-22	475-152-7630
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
815	2830313305	text/plain	18786.4297	2018-12-23	3545822686710000	Vkrmymqjxfppgyupjvgaw
141	704993243	text/plain	12917.5303	2019-01-22	3535688087990000	Itkjlfdiqlsqsheektqyu
15351	1669411268	text/plain	82096.5312	2019-02-24	3542466010800000	Wvucjatdotughtjydifxh
1561	4113394080	text/plain	72164.7891	2018-08-12	3569338224050000	Yqqblgznmgzlztljiyjtr
316	3708100840	text/plain	53897.1914	2018-04-19	3586660333050000	Rbrftkmhrznbrhajxbrxh
32511	9047169943	text/plain	54716.7695	2018-12-13	30365110892700	Uojxnotidqtnlztcvkapy
212	7422123097	text/plain	79479.2969	2018-09-25	4905652105280000000	Lnbquklekmoqsgdwkogxi
928	786241503	text/plain	17771.1406	2018-09-10	3565138551730000	Ksmkxjzribzsoevahouex
2396	5957743325	text/plain	95720.5312	2018-09-13	5641821069610000	Trizlcebdclvbostonwhj
92962	4234079690	text/plain	10925.79	2018-02-06	5100177121940000	Ozqbgrshvzhibocjocikk
42264	5744646100	text/plain	93896.9766	2019-02-18	3559383314010000	Btcbhdifdbgkajphdslhm
724	1752073605	text/plain	12703.6904	2019-01-16	3573216731770000	Lpiinbfirgihmcmpimseg
3573	9158807215	text/plain	48313.3789	2018-05-17	633110105830000000	Qreafdsqmixyfciteymyx
8553	9641466717	text/plain	17727.1699	2018-12-26	4905197192290000	Qblqdbapbrqrjuhjmnnhv
8353	8188461563	text/plain	77089	2018-11-29	3582940702190000	Tjgxugjqccmusboixckpg
64535	9748188880	text/plain	98380.2188	2018-05-20	5048372224310000	Zqhocdokjgvcuankiglcx
8435	7994053538	text/plain	21905.6895	2018-04-26	67094986702100000	Akmvzukaooguxytmillyf
8458	3056745187	text/plain	69684.6562	2018-10-29	3575871036560000	Rwdqvmvvxdnwlojyyxvhr
964	6628427700	text/plain	22621.7598	2018-09-18	6759263453300000000	Iodmrcdphdisgswbqgylz
3577	5851343685	text/plain	77418.0781	2019-03-25	374622624522000	Hoxhyfhlletwkyjvlntke
816	1100282730	text/plain	18787.4297	2018-12-23	372301029043000	Vywzgcktlpzcerlabrydx
142	8785705774	text/plain	12918.5303	2019-01-22	3545822686710000	Zqbirugexrmchfyqxryot
15352	55373842	text/plain	82097.5312	2019-02-24	3535688087990000	Obldpcslwtdgxlruppuah
1562	3404241701	text/plain	72165.7891	2018-08-12	3542466010800000	Bkndbrqmydceecsmqdgqf
317	5883081259	text/plain	53898.1914	2018-04-19	3569338224050000	Cqjyowwxlehfdjuolhfmm
32512	9209749490	text/plain	54717.7695	2018-12-13	3586660333050000	Rjcnbkchiwcstvpeljdpd
213	3088188814	text/plain	79480.2969	2018-09-25	30365110892700	Hqzovhwfzmsjiiejrfirf
929	7117091442	text/plain	17772.1406	2018-09-10	4905652105280000000	Cgbguzrqmjzvdcicefheq
2397	2459779308	text/plain	95721.5312	2018-09-13	3565138551730000	Ylzanqqrzlnlfenckocpp
92963	7671028175	text/plain	10926.79	2018-02-06	5641821069610000	Yqbmdcaliwpuoyfkadidt
42265	9723348242	text/plain	93897.9766	2019-02-18	5100177121940000	Itxargudltlziionxukzc
725	7651579499	text/plain	12704.6904	2019-01-16	3559383314010000	Hootbweruadrioczueuyv
3574	3682770744	text/plain	48314.3789	2018-05-17	3573216731770000	Xwwugkjtgiawguckcuhyj
8554	9531217737	text/plain	17728.1699	2018-12-26	633110105830000000	Eitpqbqwvbkpcdxlgpgyg
8354	6889041286	text/plain	77090	2018-11-29	4905197192290000	Yphrrgiohxlzizfptwait
64536	8164867765	text/plain	98381.2188	2018-05-20	3582940702190000	Rftopdogvjehznvjybgha
8436	394143030	text/plain	21906.6895	2018-04-26	5048372224310000	Cktsnzocjrjownchmpvly
8459	5726466162	text/plain	69685.6562	2018-10-29	67094986702100000	Nkchivuyuocghwvothxxx
965	8936287113	text/plain	22622.7598	2018-09-18	3575871036560000	Lchoaqnzleygfnjkpnurf
3578	4367393406	text/plain	77419.0781	2019-03-25	6759263453300000000	Azpfgxskyddjrihurhquh
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: toysrent; Owner: db2018054
--

COPY toysrent.status (nama, deskripsi) FROM stdin;
Devshare	text/plain
Avaveo	text/plain
Skajo	text/plain
Brainsphere	text/plain
Snaptags	text/plain
Chatterpoint	text/plain
\.


--
-- Name: barang_dikirim barang_dikirim_pk; Type: CONSTRAINT; Schema: public; Owner: db2018054
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pk PRIMARY KEY (no_resi, no_urut);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: pemesanan tambah_poin_pemesanan_trigger; Type: TRIGGER; Schema: toysrent; Owner: db2018054
--

CREATE TRIGGER tambah_poin_pemesanan_trigger AFTER INSERT OR DELETE OR UPDATE ON toysrent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toysrent.tambah_poin_pemesanan();


--
-- Name: barang_pesanan update_kondisi_trigger; Type: TRIGGER; Schema: toysrent; Owner: db2018054
--

CREATE TRIGGER update_kondisi_trigger AFTER INSERT ON toysrent.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE toysrent.update_kondisi();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toysrent.pengguna(no_ktp);


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toysrent.anggota(no_ktp);


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES toysrent.level_keanggotaan(nama_level);


--
-- Name: anggota anggota_level_fkey1; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.anggota
    ADD CONSTRAINT anggota_level_fkey1 FOREIGN KEY (level) REFERENCES toysrent.level_keanggotaan(nama_level);


--
-- Name: anggota anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toysrent.pengguna(no_ktp);


--
-- Name: anggota anggota_no_ktp_fkey1; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey1 FOREIGN KEY (no_ktp) REFERENCES toysrent.pengguna(no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toysrent.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toysrent.pengembalian(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_id_barang_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toysrent.barang(id_barang);


--
-- Name: barang_dikirim barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toysrent.pengiriman(no_resi);


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toysrent.item(nama);


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES toysrent.anggota(no_ktp);


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toysrent.barang(id_barang);


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toysrent.pemesanan(id_pemesanan);


--
-- Name: barang_pesanan barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES toysrent.status(nama);


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES toysrent.admin(no_ktp);


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toysrent.anggota(no_ktp);


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toysrent.barang(id_barang);


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES toysrent.level_keanggotaan(nama_level);


--
-- Name: kategori_item kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toysrent.item(nama);


--
-- Name: kategori_item kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES toysrent.kategori(nama);


--
-- Name: kategori kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES toysrent.kategori(nama);


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES toysrent.anggota(no_ktp);


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES toysrent.status(nama);


--
-- Name: pengembalian pengembalian_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengembalian
    ADD CONSTRAINT pengembalian_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toysrent.pemesanan(id_pemesanan);


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toysrent.alamat(no_ktp_anggota, nama);


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toysrent.pengiriman(no_resi);


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toysrent.pemesanan(id_pemesanan);


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toysrent; Owner: db2018054
--

ALTER TABLE ONLY toysrent.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toysrent.alamat(no_ktp_anggota, nama);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

