CREATE OR REPLACE FUNCTION edit_level()
RETURNS "trigger" AS
$$

BEGIN
	IF (TG_OP = 'INSERT') THEN
UPDATE anggota SET nama_level = NEW.nama_level,
poin = NEW.poin
WHERE poin >= level_keanggotaan.minimal_poin;
RETURN NEW;

	ELSIF (TG_OP = 'UPDATE') THEN
UPDATE anggota SET nama_level = NEW.nama_level,
poin = poin + NEW.poin
WHERE poin >= level_keanggotaan.minimal_poin;

UPDATE anggota set nama_level = OLD.nama_level,
poin = poin - OLD.poin
WHERE poin >= level_keanggotaan.minimal_poin;
RETURN NEW;

END IF;
END;
$$
LANGUAGE plpgsql;