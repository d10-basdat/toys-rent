from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render


# Create your views here.
from django.views.decorators.csrf import csrf_exempt


def tambah_item(request):
    return render(request, 'tambah.html')

def update_item(request):
    return render(request, 'update.html')

def daftar_item(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent,public')
    cursor.execute("select * from kategori_item")
    dataitem = cursor.fetchall()
    response['item'] = dataitem
    return render(request, 'daftar.html', response)

def postTambahItem(request):
    if (request.method == "POST"):
        nama = request.POST['nama']
        deskripsi = request.POST['deskripsi']
        minage = request.POST['minage']
        maxage = request.POST['maxage']
        bahan = request.POST['bahan']
        kategori = request.POST['kategori']

        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent,public')

        values = "'" + nama + "','" + deskripsi + "','" + minage + "','" + maxage + "','" + bahan + "'"
        query = "INSERT INTO item (nama,deskripsi,usia_dari,usia_sampai,bahan) values (" + values + ")"
        cursor.execute(query)

        values = "'" + nama + "','" + kategori + "'"
        query = "INSERT INTO kategori_item (nama_item,nama_kategori) values (" + values + ")"
        cursor.execute(query)
        return HttpResponseRedirect('/item/daftar')
    else:
        return HttpResponseRedirect('/item/tambah')


@csrf_exempt
def delete_item(request):
    if(request.method=="POST"):
        id_item = request.POST['nama']
        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent, public')
        cursor.execute("DELETE FROM kategori_item WHERE nama_item='"+id_item+"';")
        cursor.execute("DELETE FROM item WHERE nama='"+id_item+"';")
        print("a")
        return HttpResponseRedirect('/item/daftar')
    else:
        print("b")
        return HttpResponseRedirect('/item/daftar')


