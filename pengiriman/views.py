from django.shortcuts import render
from .models import Pengiriman
from django.db import connection
import random
from datetime import datetime
from django.contrib import messages
from django.http import HttpResponseRedirect
# Create your views here.



# Create your views here.
# def index(request):
#     response = {}
#     with connection.cursor() as cursor:
#         cursor.execute("SELECT * FROM barang")
#         response['barang'] = cursor.fetchall()
#     return render(request, 'index.html', response)

def list_pengiriman(request):
    response = {}
    with connection.cursor() as cursor:
        if (request.session['role'] == 'user'):
            cursor.execute("SELECT P.no_resi, P.id_pemesanan,sum(P.ongkos + S.harga_sewa), P.tanggal, B.nama_item FROM Pengiriman P INNER JOIN PEMESANAN S ON P.id_pemesanan = S.id_pemesanan INNER JOIN BARANG_DIKIRIM I ON P.no_resi = I.no_resi INNER JOIN BARANG B ON I.id_barang = B.id_barang GROUP BY P.no_resi, B,nama_item WHERE P.no_ktp_anggota = '" + request.session['ktp'] + "'")
            response['pengiriman'] = cursor.fetchall()
        else :
            cursor.execute("SELECT P.no_resi, P.id_pemesanan,sum(P.ongkos + S.harga_sewa), P.tanggal, B.nama_item FROM Pengiriman P INNER JOIN PEMESANAN S ON P.id_pemesanan = S.id_pemesanan INNER JOIN BARANG_DIKIRIM I ON P.no_resi = I.no_resi INNER JOIN BARANG B ON I.id_barang = B.id_barang GROUP BY P.no_resi, B,nama_item")
            response['pengiriman'] = cursor.fetchall()

    return render(request, 'list_pengiriman.html', response)
    # return render(request, 'list_pengiriman.html')

def update_pengiriman(request):
    return render(request, 'update_pengiriman.html')
#
def buat_pengiriman(request):
#     response = {}
#     if request.method == 'POST':
#         no_resi = 0
#         cursor = connection.cursor()
#         cursor.execute("SELECT no_resi FROM PENGIRIMAN")
#         no_resi_all = cursor.fetchall()
#         list_no_resi = list(no_resi_all[0])
#         while no_resi in list_no_resi:
#             no_resi = random.randrange(0,9999999999)
#         nama_item = request.POST['barang']
#         datenow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#         cursor.execute("SELECT * FROM ALAMAT")
#         response['alamat_tujuan'] = cursor.fetchall()
#         tanggal = request.POST['tanggal']
#         metode = request.POST['metode']
#         alamat = request.POST['alamat']
#         query = "INSERT INTO PENGIRIMAN VALUES('{0}','{1}',''{2}','{3}','{4}','{5}','{6}');".format(
#             no_resi, null, metode, 30000, tanggal, request.session['ktp'], alamat,
#
#         )
#     else:
#         with connection.cursor() as cursor:
#             cursor.execute("SELECT * FROM PENGGUNA")
#             response['pengguna'] = cursor.fetchall()
#             cursor.execute("SELECT * FROM PENGIRIMAN")
#             response['pengiriman'] = cursor.fetchall()
#
#     cursor = connection.cursor()
#     cursor.execute("SELECT * FROM ALAMAT")
#     response['alamat_tujuan'] = cursor.fetchall()
#     cursor.execute("SELECT nama_item FROM BARANG B, BARANG_PESANAN BP WHERE BP.id_barang = B.id_barang")
#     response['barang_pesanan'] = cursor.fetchall()

    response = {}
    cursor = connection.cursor()
    cursor.execute("SELECT nama_item FROM BARANG B, BARANG_PESANAN BP WHERE BP.id_barang = B.id_barang")
    response['barang_pesanan'] = cursor.fetchall()
    no_ktp = request.session.get('ktp')
    cursor.execute("SELECT * FROM ALAMAT A WHERE A.no_ktp_anggota = " +"'"+no_ktp+"'")
    response['alamat_tujuan'] = cursor.fetchall()
    return render(request, 'buat_pengiriman.html', response)

def buatPengirimanFormSubmit(request):
    if (request.method == "POST"):
        no_resi = random.randrange(0,9999999999)
        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent,public')
        cursor.execute("SELECT no_resi FROM PENGIRIMAN")
        no_resi_all = cursor.fetchall()
        while no_resi in no_resi_all:
            no_resi = random.randrange(0,9999999999)
        no_resi = str(no_resi)
        nama_item = request.POST.get('barang')
        datenow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        tanggal = request.POST.get('tanggal')
        metode = request.POST.get('metode')
        alamat = request.POST.get('alamat')


        values = "'" + no_resi + "','" + "9047169943" + "','" + metode + "','" + "30000" + "','" + tanggal + "','" + request.session['ktp'] + "','" + alamat + "'"
        query = "INSERT INTO PENGIRIMAN (no_resi,id_pemesanan,metode,ongkos,tanggal,no_ktp_anggota,nama_alamat_anggota) values (" + values + ")"
        cursor.execute(query)

        return HttpResponseRedirect('/pengiriman/')
    else:
        return HttpResponseRedirect('/pengiriman/buat/')
