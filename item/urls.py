from django.conf.urls import url
from .views import *

urlpatterns = [
    url('tambah', tambah_item, name="tambah"),
    url('daftar', daftar_item, name="daftar"),
    url('postitem', postTambahItem, name="postitem"),
    url('delete_item', delete_item, name="delete_item")
]
