from django.conf.urls import url
from .views import tambah_barang, daftar_barang

urlpatterns = [
    url('tambah_barang', tambah_barang, name="tambah_barang"),
    url('daftar_barang', daftar_barang, name="daftar_barang"),
]
