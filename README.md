# Basdat Assignment
Repository for Basdat Group Assignment

## Our group's members
- M Fijar Lazuardy R E - 1706039471
- Falya Aqiela Sekardina - 1706027673
- Abraham Williams Lumbantobing - 1706039414
- Nabilla Yuli Shafira - 1706984682

## App status
[![Pipeline](https://gitlab.com/d10-basdat/toys-rent/badges/master/pipeline.svg)](https://gitlab.com/d10-basdat/toys-rent/commits/master)

## We are using
- Django v2.1.5
- PostgreSQL for DBMS
