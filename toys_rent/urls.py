"""toys_rent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, re_path, include
from toysrent import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('toysrent.urls')),
    path('pengiriman/', include('pengiriman.urls')),
    path('review/', include('review.urls')),
    re_path('registrasi/', include('registrasi.urls')),
    re_path('item/', include('item.urls')),
    re_path('barang/', include('barang.urls')),
    re_path('chat/', include('chat.urls')),
    path('pesanan/', include('pesanan.urls')),

    re_path(r'^$', views.index),
    path('list-level/', views.list_level),
	path('new-level', views.new_level, name="new_level"),
    path('update-level/', views.update_level, name="update_level")

]
