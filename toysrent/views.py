from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import connection

# Create your views here.
def index(request):
    return render(request, 'index.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')

def list_level(request):
    query = "SELECT * FROM level_keanggotaan;"
    cursor = connection.cursor()
    cursor.execute(query)
    list_level = cursor.fetchall()
    print(list_level)
    return render(request, 'list_level.html', {'level': list_level, 'counter': 1})

def profile(request):
    return render(request, 'profil.html')

def new_level(request):
    response = {}
    cursor = connection.cursor()
    if request.method == "POST":
        nama_level = request.POST['nama-level']
        min_poin = request.POST['min-poin']
        deskripsi = request.POST['deskripsi']
        ins = "INSERT INTO level_keanggotaan VALUES('{0}', {1}, '{2}');".format(nama_level, min_poin, deskripsi)
        print(ins)
        cursor.execute(ins)
        HttpResponseRedirect('/list-level')

    return render(request, 'new_level.html')

def update_level(request, nama_level):
    cursor = connection.cursor()
    if request.method =="POST":
        level = request.POST['nama-level']
        poin = request.POST['min-poin']
        desk = request.POST['deskripsi']
        cursor.execute("UPDATE level_keanggotaan SET minimum_poin={0}, deskripsi='{1}' WHERE nama_level='{2}'".format(poin, desk, level))
        return render(request, 'index.html')
    else:
        cursor.execute("SELECT nama_level FROM level_keanggotaan WHERE nama_level='{0}'".format(nama_level))
        level = cursor.fetchone()
        return render(request, 'update_level.html', {'level' : level})
        
def delete_level(request):
    # cursor = connection.cursor()
    # cursor.execute("DELETE FROM level_keanggotaan WHERE nama_level='{}'".format(nama_level))
    HttpResponseRedirect('list-level/')

def postLogin(request):
    ktp = str(request.POST['ktp'])
    email = request.POST['email']

    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent,public')
    cursor.execute("SELECT * from pengguna where no_ktp='"+ktp+"' and email='"+email+"'")
    datalogin = cursor.fetchone();
    if datalogin:
        cursor.execute("SELECT * from admin where no_ktp='"+ktp+"'")
        dataadmin = cursor.fetchone();
        if dataadmin:
            cursor.execute("SELECT nama_lengkap from pengguna where no_ktp='"+ktp+"'")
            namaadmin = cursor.fetchall()
            for nama in namaadmin:
                namaadmin = nama[0]

            cursor.execute("SELECT tanggal_lahir from pengguna where no_ktp='" + ktp + "'")
            tanggal = cursor.fetchone()
            tanggal = str(tanggal[0])

            cursor.execute("SELECT no_telp from pengguna where no_ktp='" + ktp + "'")
            notelp = cursor.fetchall()
            for row in notelp:
                notelp = row[0]

            request.session['nama'] = namaadmin
            request.session['ktp'] = ktp
            request.session['email'] = email
            request.session['tanggal'] = tanggal
            request.session['notelp'] = notelp
            request.session['role'] = 'admin'
            return HttpResponseRedirect('/pesanan/')
        else:
            cursor.execute("SELECT * from anggota where no_ktp='"+ktp+"'")
            datauser = cursor.fetchone()
            if datauser:
                cursor.execute("SELECT nama_lengkap from pengguna where no_ktp='"+ktp+"'")
                namauser = cursor.fetchall()
                for nama in namauser:
                    namauser = nama[0]

                cursor.execute("SELECT no_telp from pengguna where no_ktp='" + ktp + "'")
                notelp = cursor.fetchall()
                for row in notelp:
                    notelp = row[0]

                cursor.execute("SELECT tanggal_lahir from pengguna where no_ktp='" + ktp + "'")
                tanggal = cursor.fetchone()
                tanggal = str(tanggal[0])

                cursor.execute("SELECT level from anggota where no_ktp='" + ktp + "'")
                level = cursor.fetchall()
                for row in level:
                    level = row[0]

                cursor.execute("SELECT poin from anggota where no_ktp='" + ktp + "'")
                poin = cursor.fetchall()
                for row in poin:
                    poin = row[0]

                # cursor.execute("SELECT * from alamat where no_ktp_anggota='" + ktp + "'")
                # alamat = cursor.fetchall()
                # for row in alamat:
                #     alamat+=str(row)

                request.session['nama'] = namauser
                request.session['ktp'] = ktp
                request.session['email'] = email
                request.session['notelp'] = notelp
                request.session['tanggal'] = tanggal
                request.session['poin'] = poin
                request.session['level'] = level
                # request.session['alamat'] = alamat
                request.session['role'] = 'user'
                # request.session['level'] = nama.level
                return HttpResponseRedirect('/barang/daftar_barang/')
    else:
        messages.error(request, "Data yang Anda masukkan salah")
        return HttpResponseRedirect('/')
