CREATE OR REPLACE FUNCTION update_ongkos()
RETURNS trigger AS
$$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE PEMESANAN SET ongkos = NEW.ongkos;

RETURN NEW;

	ELSIF (TG_OP = 'UPDATE') THEN
UPDATE PEMESANAN SET ongkos = NEW.ongkos,
ongkos = ongkos + NEW.ongkos;

UPDATE PEMESANAN SET ongkos = OLD.ongkos,
ongkos = ongkos - OLD.ongkos;

RETURN NEW;

END IF;
END;
$$
LANGUAGE plpgsql;
