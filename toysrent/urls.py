from django.conf.urls import url
from django.urls import path, re_path
from .views import *

urlpatterns = [
    re_path(r'^$', index, name='landingPage'),
    url('list-level', list_level, name="list_level"),
    path('update-level/<str:nama_level>/', update_level, name="update_level"),
    url('new-level', new_level, name="new_level"),
    # url(r'^delete-level/(?P<string>[\w\-]+)/$', delete_level, name=delete_level),
    url('delete-level/<str:nama_level>/', delete_level, name="delete_level"),
    url('logout', logout, name='logout'),
    url('postlogin', postLogin, name='postlogin'),
    url('profile', profile, name='profile')
    # url('profile', profile, name='datapengguna')
]
