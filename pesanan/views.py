from django.shortcuts import render
from django.db import connection
import random
from datetime import datetime
from django.shortcuts import redirect
# Create your views here.
def list_pesanan(request):
    user = {}
    response = {}
    lst = []
    user['ktp_user'] = request.session['ktp']
    user['nama'] = request.session['nama']
    user['email'] = request.session['email']
    user['role'] = request.session['role']
    response['user'] = user
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM PEMESANAN WHERE no_ktp_pemesan='{0}'".format(user['ktp_user']))
    pemesanan = cursor.fetchall()
    for p in pemesanan:
        cursor.execute("SELECT * FROM BARANG_PESANAN WHERE id_pemesanan='{0}'".format(p[0]))
        a = cursor.fetchall()
    response['barang_pesanan'] = a
    for i in a:
        # print(i)
        cursor.execute("SELECT * FROM BARANG WHERE id_barang='{0}'".format(i[2]))
        response['barang'] = cursor.fetchall()
        

    response['pemesanan'] = pemesanan
    response['counter'] = 1
    return render(request, 'list_pesanan.html', response)

def buat_pesanan(request):
    response = {}
    if request.method == 'POST':
        id_pemesanan = 0
        cursor = connection.cursor()
        cursor.execute("SELECT id_pemesanan FROM PEMESANAN")
        id_pemesanan_all = cursor.fetchall()
        list_id_pemesanan = list(id_pemesanan_all[0])
        while id_pemesanan in list_id_pemesanan:
            id_pemesanan = random.randrange(0,9999999999)
        id_pengguna = request.POST['pengguna']
        datenow = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        kuantitas = 1
        id_barang = request.POST['barang']
        hari = request.POST['hari']
        cursor.execute("SELECT harga_sewa FROM INFO_BARANG_LEVEL WHERE id_barang='{0}'".format(id_barang))
        harga = cursor.fetchone()
        print(harga[0])
        status = "Brainsphere"
        query = "INSERT INTO PEMESANAN VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}');".format(
            id_pemesanan,
            datenow,
            kuantitas,
            harga[0],
            1000.0,
            id_pengguna,
            status
        )
        cursor.execute(query)
        return redirect('/')
    else:
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM PENGGUNA")
            response['pengguna'] = cursor.fetchall()
            cursor.execute("SELECT * FROM BARANG")
            response['barang'] = cursor.fetchall()
    return render(request, 'buat_pesanan.html', response)

def update_pesanan(request):
    return render(request, 'update_pesanan.html')
