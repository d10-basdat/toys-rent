CREATE OR REPLACE FUNCTION update_kondisi()
RETURNS trigger AS $$

BEGIN 
IF (TG_OP = 'INSERT') THEN
UPDATE BARANG
SET kondisi = 'sedang disewa anggota'
WHERE id_barang = NEW.id_barang;
RETURN NEW;

END IF;
END;
$$
LANGUAGE plpgsql;