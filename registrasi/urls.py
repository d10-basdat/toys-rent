from django.conf.urls import url
from django.urls import re_path

from .views import *

urlpatterns = [
    url('roleuser', ketiga, name="roleuser"),
    url('roleadmin', kedua, name="roleadmin"),
    url('postadmin', postSignupAdmin, name="postadmin"),
    url('postuser', postSignupUser, name="postuser"),
    re_path(r'^$', utama),
]