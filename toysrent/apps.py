from django.apps import AppConfig


class ToysrentConfig(AppConfig):
    name = 'toysrent'
