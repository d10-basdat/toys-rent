# Generated by Django 2.1.5 on 2019-05-27 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('toysrent', '0002_auto_20190515_0609'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pengiriman',
            fields=[
                ('no_resi', models.CharField(max_length=10, primary_key=True, serialize=False)),
                ('id_pemesanan', models.CharField(max_length=10)),
                ('metode', models.TextField()),
                ('ongkos', models.FloatField()),
                ('tanggal', models.DateField()),
                ('no_ktp_anggota', models.CharField(max_length=20)),
                ('nama_alamat_anggota', models.CharField(max_length=255)),
            ],
        ),
    ]
