CREATE TRIGGER tambah_poin_pemesanan_trigger
AFTER INSERT OR UPDATE OR DELETE
ON PEMESANAN
FOR EACH ROW
EXECUTE PROCEDURE tambah_poin_pemesanan();
