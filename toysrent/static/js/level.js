$(document).on("click", "#del-btn", function () {
    var myBookId = $(this)[0].value;
    console.log(myBookId);
    $('#delete-yes').attr('value', myBookId)
    // As pointed out in comments, 
    // it is unnecessary to have to manually call the modal.
    // $('#addBookDialog').modal('show');
});