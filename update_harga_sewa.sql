CREATE OR REPLACE FUNCTION update_harga_sewa()
RETURNS trigger AS
$$
BEGIN
IF (TG_OP = 'INSERT') THEN
UPDATE PEMESANAN SET harga_sewa = NEW.harga_sewa
WHERE harga_sewa = harga_sewa_item;
RETURN NEW;
	END IF;
END;
$$
LANGUAGE plpgsql;
