from django.shortcuts import render
from django.db import connection

# Create your views here.
def buat_review(request):
    return render(request, 'buat_review.html')

def update_review(request):
    return render(request, 'update_review.html')

def list_review(request):
    response = {}
    with connection.cursor() as cursor:
        if (request.session['role'] == 'user'):
            cursor.execute("SELECT P.no_resi, B.nama_item, P.tanggal_review, P.review FROM Barang_dikirim P INNER JOIN Barang B ON B.id_barang = P.id_barang WHERE P.no_ktp_anggota = '" + request.session['ktp'] +"'")
            response['review'] = cursor.fetchall()
        else :
            cursor.execute("SELECT P.no_resi, B.nama_item, P.tanggal_review, P.review FROM Barang_dikirim P INNER JOIN Barang B ON B.id_barang = P.id_barang")
            response['review'] = cursor.fetchall()
    return render(request, 'list_review.html', response)
    # return render(request, 'list_pengiriman.html')
