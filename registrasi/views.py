from django.contrib import messages
from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
def utama(request):
    return render(request, 'chooseRole.html')

def kedua(request):
    # admin
    return render(request, 'signupPage.html')

def ketiga(request):
    return render(request, 'roleuser.html')

def postSignupAdmin(request):
    ktp = str(request.POST['ktp'])

    if(len(ktp) != 9):
        messages.error(request, "Gagal: NO KTP harus memiliki 9 digit")
        return HttpResponseRedirect('/registrasi/roleadmin/')

    else:
        namalengkap = request.POST['nama']
        email = request.POST['email']
        nohp = request.POST['nohp']
        birthdate = request.POST['ultah']

        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("SELECT * from admin where no_ktp='"+ktp+"'")
        hasil_ktp = cursor.fetchone();
        if(hasil_ktp):
            if len(hasil_ktp)>0:
                messages.error(request, "Gagal: KTP "+ktp+" sudah terdaftar pada sistem")
                return HttpResponseRedirect('/registrasi/roleadmin/')

        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("SELECT * from pengguna where email='"+email+"'")
        hasil_email = cursor.fetchone();
        if(hasil_email):
            if len(hasil_email)>0:
                messages.error(request, "Gagal: Email "+email+" sudah terdaftar pada sistem")
                return HttpResponseRedirect('/registrasi/roleadmin/')

        values = "'" + ktp + "','" + namalengkap + "','" + email + "','" + birthdate + "','" + nohp + "'"
        query = "INSERT INTO pengguna (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)

        values = ktp
        query = "INSERT INTO admin (no_ktp) values (" + values + ")"
        cursor.execute(query)

        messages.success(request, "Register berhasil atas nama: "+namalengkap+
                         "Silahkan lakukan login pada halaman depan")
        return HttpResponseRedirect('/')

def postSignupUser(request):
    ktp = str(request.POST['ktp'])

    if(len(ktp) != 9):
        messages.error(request, "Gagal: NO KTP harus memiliki 9 digit")
        return HttpResponseRedirect('/registrasi/roleadmin/')

    else:
        namalengkap = request.POST['nama']
        email = request.POST['email']
        nohp = request.POST['nohp']
        birthdate = request.POST['ultah']
        jenis = request.POST['jenis']
        jalan = request.POST['jalan']
        norumah = request.POST['norumah']
        kota = request.POST['kota']
        kodepos = request.POST['kodepos']
        poin = str(0)
        level = 'Bronze'

        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("SELECT * from anggota where no_ktp='"+ktp+"'")
        hasil_ktp = cursor.fetchone();
        if(hasil_ktp):
            if len(hasil_ktp)>0:
                messages.error(request, "Gagal: KTP "+ktp+" sudah terdaftar pada sistem")
                return HttpResponseRedirect('/registrasi/roleuser/')

        cursor = connection.cursor()
        cursor.execute('set search_path to toysrent')
        cursor.execute("SELECT * from pengguna where email='"+email+"'")
        hasil_email = cursor.fetchone();
        if(hasil_email):
            if len(hasil_email)>0:
                messages.error(request, "Gagal: Email "+email+" sudah terdaftar pada sistem")
                return HttpResponseRedirect('/registrasi/roleuser/')

        values = "'" + ktp + "','" + namalengkap + "','" + email + "','" + birthdate + "','" + nohp + "'"
        query = "INSERT INTO pengguna (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)

        # deskripsi = 'banana strawberry chocochip pancakes'
        # values = "'" + level + "','" + poin + "','" + deskripsi + "'"
        # query = "INSERT INTO level_keanggotaan (nama_level,minimum_poin,deskripsi) values (" + values + ")"
        # cursor.execute(query)

        values = "'" + ktp + "','" + poin + "','" + level + "'"
        query = "INSERT INTO anggota (no_ktp,poin,level) values (" + values + ")"
        cursor.execute(query)

        values = "'" + ktp + "','" + jenis + "','" + jalan + "','" + norumah + "','" + kota + "','" + kodepos + "'"
        query = "INSERT INTO alamat (no_ktp_anggota,nama,jalan,nomor,kota,kodepos) values (" + values + ")"
        cursor.execute(query)

        messages.success(request, "Register berhasil atas nama: "+namalengkap+
                         "Silahkan lakukan login pada halaman depan")
        return HttpResponseRedirect('/')